/* eslint-disable prettier/prettier */
const mongoose = require("mongoose");

const schema = new mongoose.Schema(
  {
    userid: { type: String, default: "" },
    quota_number: { type: Number },
    max_value: { type: Number }
  },
  {
    timestamps: true,
  }
);

const QuotaDocs = mongoose.model("QuotaDocs", schema);

module.exports = QuotaDocs;