const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  _id: {
    type: String, // Assuming the Keycloak user ID is of type string
    required: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    trim: true,
  },  
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true

  },
  password: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
    required: true,
    trim: true,
  },
  lastName: {
    type: String,
    required: true,
    trim: true,
  },
  mobileNumber: {
    type: String,
    required: true,
    trim: true,
  },
  birthDate: {
    type: Date,
    required: true,
  }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
