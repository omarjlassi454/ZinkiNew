/* eslint-disable prettier/prettier */
const router = require('express').Router();
const express = require('express');
const AuthController = require('../Controllers/AuthController');



router.post('/signup',AuthController.register);
router.post('/login',AuthController.login);

module.exports = router;
