/* eslint-disable prettier/prettier */
const router = require('express').Router();
const express = require('express');
const GenerateSSmlController = require('../Controllers/GenerateSSmlController');

const OCR = require('../Controllers/OCR');

router.post('/Quota',GenerateSSmlController.Quota);
//router.get('/audio',GenerateSSmlController.audio);

router.post('/test',GenerateSSmlController.TesteSsml);
router.get('/QuotaID/:id',GenerateSSmlController.getQuotaById);
router.post('/UpdateTags', GenerateSSmlController.UpdateTags);
router.post('/DisplayTags', GenerateSSmlController.DisplayTags); 
router.post('/sentogoogle', GenerateSSmlController.SenToGoogle); 
router.post('/TesteOCR', OCR.TesteOCR);
router.post('/QuotaDocs', OCR.checkquota);
router.post('/Updatequota', OCR.Updatequota);
router.post('/Updatequotaafterpayment', OCR.Updatequotaafterpayment);
router.post('/Updatequotamaxvalue', OCR.Updatequotamaxvalue);
router.get('/quotas/:id', OCR.getDocQuotaById);
router.get('/user/:id', OCR.getUserById);
module.exports = router;
