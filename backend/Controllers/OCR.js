const QuotaDocs = require('../models/QuotaDocs');
const User = require('../models/User');
exports.checkquota = async (req, res) => {try {
    const { id }= req.body
    console.log('id1 : ', id)

    const quota = await QuotaDocs.findOne({ userid: id }); // Use QuotaID instead of id
    console.log('id : ', id)
    if (!quota) {
        res.send({ message: 'Quota not found' });
      // Return a 404 error if no matching quota is found
      console.log('Quota not found');
      return;
    }

     else if (quota.quota_number >= quota.max_value) {
      res.send({ message: 'you must charge your account' });
      console.log('you must charge your account');
     

    }

  

    else {
      console.log('everything is OK..!')      
        res.send({ message: 'quota checked' });
    } }  catch (error) {
        console.error('Error processing document:', error);
        res.status(500).json({ error: 'An error occurred while processing the document' });
    }
};

exports.Updatequota = async (req, res) => { try {
    const { id }= req.body
    console.log('id1 : ', id)
    await QuotaDocs.updateOne({ userid: id }, { $inc: { quota_number: 1 } });
    console.log("quota updated");
    res.send({ message: 'quota updated' });
    

}  catch (error) {
    console.error('Error Updating quota:', error);
    res.status(500).json({ error: 'An error occurred while processing the quota' });
}
};

exports.Updatequotaafterpayment = async (req, res) => { try {
    const { id , number }= req.body
    console.log('id1 : ', id)
    await QuotaDocs.updateOne({ userid: id }, { $inc: { quota_number: number } });
    console.log("quota updated");
    res.send({ message: 'quota updated' });
    

}  catch (error) {
    console.error('Error Updating quota:', error);
    res.status(500).json({ error: 'An error occurred while processing the quota' });
}
};


exports.Updatequotamaxvalue = async (req, res) => {
    try {
      const { id, number } = req.body;
      console.log('id1:', id);
  
      // Update the max_value field
      await QuotaDocs.updateOne(
        { userid: id },
        { $inc: { max_value: number } } // Use $inc operator to increment the max_value field by number
      );
  
      console.log('max_value updated');
      res.send({ message: 'Quota updated' });
    } catch (error) {
      console.error('Error updating quota:', error);
      res.status(500).json({ error: 'An error occurred while processing the quota' });
    }
  };
  
exports.TesteOCR = async (req, res) => {
    try {
        const { DocumentProcessorServiceClient } = require('@google-cloud/documentai').v1;

        const projectId = '997319559032';
        const location = 'us'; // Format is 'us' or 'eu'
        const processorId = 'b438d0e755a5aa36'; // Create processor in Cloud Console

        const client = new DocumentProcessorServiceClient();
        const name = `projects/${projectId}/locations/${location}/processors/${processorId}`;

        const {encodedImage , id }= req.body
        console.log('id1 : ', id)

        const quota = await QuotaDocs.findOne({ userid: id }); // Use QuotaID instead of id
        console.log('id : ', id)
        if (!quota) {
          // Return a 404 error if no matching quota is found
          console.log('Quota not found');
          return;
        }
    
         if (quota.quota_number >= quota.max_value) {
          res.send({ message: 'you must charge your account' });
          console.log('you must charge your account');
         
    
        }
    
      
    
        else {
          console.log('everything is OK..!')
          await QuotaDocs.updateOne({ userid: id }, { $inc: { quota_number: 1 } });

        const request = {
            name,
            rawDocument: {
                content: encodedImage,
                mimeType: 'image/jpeg',
            },
        };

        const [result] = await client.processDocument(request);
        const { document } = result;
        const { text } = document;

        const getText = textAnchor => {
            if (!textAnchor.textSegments || textAnchor.textSegments.length === 0) {
                return '';
            }

            const startIndex = textAnchor.textSegments[0].startIndex || 0;
            const endIndex = textAnchor.textSegments[0].endIndex;

            return text.substring(startIndex, endIndex);
        };

        const paragraphs = [];
        const [page1] = document.pages;

        for (const paragraph of page1.paragraphs) {
            const paragraphText = getText(paragraph.layout.textAnchor);
            paragraphs.push(paragraphText);
        }

        console.log('The document contains the following paragraphs:');
        paragraphs.forEach(paragraph => {
            console.log(`Paragraph text:\n${paragraph}`);
        });

        res.status(200).json({ paragraphs });
    } } catch (error) {
        console.error('Error processing document:', error);
        res.status(500).json({ error: 'An error occurred while processing the document' });
    }
};

/*exports.getDocQuotaById = async (req, res) => {
    const { id } = req.params;
  
    try {
      // Find the quota document with the matching ID
      const quota = await QuotaDocs.findById(id);
  
      if (!quota) {
        // Return a 404 error if no matching quota is found
        res.status(404).json({ error: 'Quota not found' });
        return;
      }
  
      // Log the quota document
      console.log(quota);
  
      // Send the quota document in the response
      res.json(quota);
    } catch (error) {
      console.error('Error retrieving quota document:', error);
      res.status(500).json({ error: 'An error occurred while retrieving quota document' });
    }
  };*/
  



  exports.getDocQuotaById = async (req, res) => {
    const { id } = req.params; // Update parameter name to QuotaID
  
    try {
      // Find the quota document with matching id
      const quota = await QuotaDocs.findOne({ userid: id }); // Use QuotaID instead of id
  
      if (quota) {
        // Return a 404 error if no matching quota is found
      //  res.send('Quota  found' , quota);
        res.send({ message: 'quota found ',quota: quota });

        return;
      }
  
      
  
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal server error');
    }
  };
  

  exports.getUserById = async (req, res) => {
    const { id } = req.params; // Update parameter name to QuotaID
  
    try {
      // Find the quota document with matching id
      const user = await User.findOne({ _id: id }); // Use QuotaID instead of id
  
      if (user) {
        // Return a 404 error if no matching quota is found
        //  res.send('Quota  found' , quota);
        res.send({ message: 'user found ',user: user });

        return;
      }
  
      
  
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal server error');
    }
  };
  