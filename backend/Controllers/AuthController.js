const IpAdress = require('./IpAdress');
const User = require('../models/User');
const Keycloak = require('keycloak-connect');
const keycloakConfig = {
  clientId: 'backend', // Client ID of your backend client in Keycloak
  bearerOnly: true, // Set this to true if your backend is a bearer-only application
  serverUrl: 'http://localhost:8080/auth', // URL of your Keycloak server
  realm: 'myrealm', // Name of your Keycloak realm
};
const keycloak = new Keycloak({}, keycloakConfig);
const Quota = require('../models/Quota');
const QuotaDocs = require('../models/QuotaDocs');

const axios = require('axios');

exports.register = async (req, res) => {
  const { email, password, firstName, lastName, username, birthDate  , mobileNumber } = req.body;
  const data = new URLSearchParams();
     data.append('username', 'jlassi14');
     data.append('password', 'admin123');
     data.append('grant_type', 'password');
     data.append('client_id', 'backend');
     data.append('client_secret', 'h2yc7GFFdAoPxplrAsCI5QYTepkH1cY9');
  data.append('scope', 'openid');
  try {

    const response1 = await axios.post('http://127.0.0.1:8080/realms/myrealm/protocol/openid-connect/token', data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });

    //console.log('Response2211111:  ', response1.data);
    const response = await axios.post(
      `http://127.0.0.1:8080/admin/realms/myrealm/users`,
      {
        username,
        email,
        enabled: true,
        credentials: [
          {
            type: 'password',
            value: password,
            temporary: false
          }
        ],
        firstName,
        lastName
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${response1.data.access_token}`
        }
      }
    );     

    response1.data.access_token
   // console.log("USERID : ", response.data.id)
    //const keycloakUser = response.data;
   // console.log('keycloakUserrrr :', keycloakUser)
   const userId = response.headers.location.split('/').pop();
   console.log('User ID:', userId);
   console.log('Keycloak API Response:', response);

    const newUser = new User({
      
      _id: userId,
      username,
      email,
      password, 
      firstName,
      lastName,
      birthDate,
      mobileNumber
    });
    const newquota = new Quota({
      
      userid: userId,
    quota_number: 0,
    quota_type: "free",
    max_value: 4000000
    });
    const newQuotaDocs = new QuotaDocs({
      
      userid: userId,
    quota_number: 0,
    max_value: 15
    });
    
    await newUser.save();
    await newquota.save();
    await newQuotaDocs.save();

    res.sendStatus(201);
  } catch (err) {
    console.error('Error registering user:', err);
    res.sendStatus(500);
  }
};



exports.login = async (req, res) => {
  const { username, password } = req.body;
  const data = new URLSearchParams();
  data.append('username', username);
  data.append('password', password);
  data.append('grant_type', 'password');
  data.append('client_id', 'backend');
  data.append('client_secret', 'h2yc7GFFdAoPxplrAsCI5QYTepkH1cY9');
  data.append('scope', 'openid');

  try {
    const response = await axios.post('http://127.0.0.1:8080/realms/myrealm/protocol/openid-connect/token', data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
    const user = await User.findOne({ username, password }); // Replace with your own query to retrieve the user details

const accessToken = response.data.access_token;
const UserId= user._id
  
    res.status(200).json({ accessToken , UserId });
    console.log('access_token;', accessToken)
  } catch (err) {
    console.error('Error logging in:', err);
    res.status(500).json({ error: 'Failed to log in' });
  }
};



