import React from 'react';
import { View } from 'react-native';
import FixText from '../components/FixText';

const FixTextScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <FixText />
    </View>
  );
};

export default FixTextScreen;