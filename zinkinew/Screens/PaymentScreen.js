import React from 'react';
import { View } from 'react-native';
import Payment from '../components/Payment';

const PaymentScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <Payment />
    </View>
  );
};

export default PaymentScreen;