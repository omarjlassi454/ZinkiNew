import React from 'react';
import { View } from 'react-native';
import AllFiles from '../components/AllFiles';

const AllFilesScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <AllFiles />
    </View>
  );
};

export default AllFilesScreen;