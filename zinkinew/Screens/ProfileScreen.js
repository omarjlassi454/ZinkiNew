import React from 'react';
import { View } from 'react-native';
import Profile from '../components/Profile';

const ProfileScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <Profile />
    </View>
  );
};

export default ProfileScreen;