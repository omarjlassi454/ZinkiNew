import React from 'react';
import { View } from 'react-native';
import DisplaySsmlConfig from '../components/DisplaySsmlConfig';

const DisplaySsmlConfigScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <DisplaySsmlConfig />
    </View>
  );
};

export default DisplaySsmlConfigScreen;