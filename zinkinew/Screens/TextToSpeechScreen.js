import React from 'react';
import { View } from 'react-native';
import TextToSpeech from '../components/TextToSpeech';

const TextToSpeechScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <TextToSpeech />
    </View>
  );
};

export default TextToSpeechScreen;