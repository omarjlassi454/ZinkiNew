import React from 'react';
import { View } from 'react-native';
import Home from '../components/Home';

const HomeScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <Home />
    </View>
  );
};

export default HomeScreen;