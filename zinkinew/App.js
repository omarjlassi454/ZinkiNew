import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SQLite from 'react-native-sqlite-storage';

import DisplaySsmlConfigScreen from './Screens/DisplaySsmlConfigScreen';
import FixTextScreen from './Screens/FixTextScreen';
import HomeScreen from './Screens/HomeScreen';
import SignUpScreen from './Screens/SignUpScreen';
import TextToSpeechScreen from './Screens/TextToSpeechScreen';
import LoginScreen from './Screens/LoginScreen';
import HistoryScreen from './Screens/HistoryScreen';
import ProfileScreen from './Screens/ProfileScreen';
import AllFilesScreen from './Screens/AllFilesScreen';
import PaymentScreen from './Screens/PaymentScreen';


import { LogBox } from 'react-native';
const Stack = createStackNavigator();

function App() {
  LogBox.ignoreAllLogs();
  const [token, setToken] = useState('');

  useEffect(() => {
    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
    db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS TOKEN (token TEXT)',
        [],
        () => {
          tx.executeSql(
            'SELECT token FROM TOKEN',
            [],
            (_, result) => {
              if (result.rows.length > 0) {
                const storedToken = result.rows.item(0).token;
                setToken(storedToken);
              }
            },
            (_, error) => {
              console.log('Error retrieving token:', error);
            }
          );
        },
        (error) => {
          console.log('Error creating table:', error);
        }
      );
    });
  }, []);

 

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={token === '0' ? 'Login' : 'Login'}>
        <Stack.Screen name="TextToSpeech" component={TextToSpeechScreen}/>
        <Stack.Screen name="History" component={HistoryScreen} />
        <Stack.Screen name="FixText" component={FixTextScreen} />
        <Stack.Screen name="DisplaySsmlConfig" component={DisplaySsmlConfigScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="SignUp" component={SignUpScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen name="AllFiles" component={AllFilesScreen} />
        <Stack.Screen name="Payment" component={PaymentScreen} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}ProfileScreen

export default App;
