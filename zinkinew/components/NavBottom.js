import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { BottomNavigation, Provider , Text} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from "@react-navigation/native";

const NavBottom = () => {
    const [activeTab, setActiveTab] = useState('home');
    const navigation = useNavigation();

    const handleTabPress = (tab) => {
        setActiveTab(tab);
    };

    return (
        <View style={styles.container}>
            <View style={styles.tabContainer}>
                <TouchableOpacity
                    onPress={() => { handleTabPress('home'); navigation.navigate('Home') }}
                    style={styles.tabButton}
                >
                    <Icon
                        name="home"
                        size={24}
                        color={activeTab === 'home' ? '#fff' : '#fff'}
                        style={styles.icon}
                    />
                    <Text style={[styles.tabText, activeTab === 'home' && styles.activeTabText]}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => { handleTabPress('AllFiles'); navigation.navigate('AllFiles') }}
                    style={styles.tabButton}
                >
                    <MaterialIcons
                        name="history"
                        size={24}
                        color={activeTab === 'history' ? '#fff' : '#fff'}
                        style={styles.icon}
                    />

                    <Text style={[styles.tabText, activeTab === 'AllFiles' && styles.activeTabText]}>All files</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Profile') }
                    style={styles.tabButton}
                >
                    <Icon
                        name="person"
                        size={24}
                        color={activeTab === 'profile' ? '#fff' : '#fff'}
                        style={styles.icon}
                    />
                    <Text style={[styles.tabText, activeTab === 'profile' && styles.activeTabText]}>Profile</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#2B3270',
        borderTopColor: '#ddd',
        borderTopWidth: 1,
        borderRadius: 50,
        width: 380,
        height:55,
        justifyContent: 'center',
        marginLeft: 15,
        marginRight: 10,
        marginBottom:10,
        
    },
    tabContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingVertical: 3,
    },
    tabButton: {
        alignItems: 'center',
    },
    icon: {
        padding: 5,
        borderRadius: 20,
        backgroundColor: 'transparent',
    },
    tabText: {
        fontSize: 12,
        color: '#fff',
        top:-8,
    },
});


export default NavBottom;
