import * as React from 'react';
import { Appbar, Provider } from 'react-native-paper';
import { View, Text, TouchableOpacity, StyleSheet, Modal, ScrollView, Image, SafeAreaView, Alert, Platform, PermissionsAndroid, ScrollViewBase, NativeModules, } from 'react-native';

const AppBar = () => (
    <Provider>
        <Appbar.Header style={styles.appBar}>
            <Appbar.BackAction />
            <Appbar.Content titleStyle={styles.title} title="Home" />
            <Appbar.Action icon="magnify" />
        </Appbar.Header>
    </Provider>
);
const styles = StyleSheet.create({

    appBar: {
        backgroundColor: '#ffffff', // Change the background color of the app bar
        elevation: 5, // Remove shadow on Android (if needed)
    },
    title: {
        fontSize: 17, // Adjust the font size as needed
        color: '#2B3270', // Change the title color
    },
})
export default AppBar;
