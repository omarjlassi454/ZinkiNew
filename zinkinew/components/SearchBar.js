import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Searchbar } from 'react-native-paper';


const SearchBar = () => {
    const [searchText, setSearchText] = useState('');
    const [isFocused, setIsFocused] = useState(false);
    const handleFocus = () => {
        setIsFocused(true);
    };

    const handleBlur = () => {
        setIsFocused(false);
    };

    const handleSearchTextChange = (text) => {
        setSearchText(text);
    };
    return (

        <Searchbar
            placeholder={isFocused ? '' : 'Search'}
            // value={value}
            //onChangeText={onChangeText}
            //onSubmitEditing={onSubmit}
            iconColor="#3E8DFB"
            inputStyle={styles.inputStyle}
            style={styles.searchBar}
            placeholderTextColor="#cccccc"
            onFocus={handleFocus}
            onBlur={handleBlur}
            onChangeText={handleSearchTextChange}

        />

    );
};

const styles = StyleSheet.create({

    searchBar: {
        backgroundColor: '#F5F5F5',
        borderRadius: 20,
        borderColor: '#3E8DFB',
        marginHorizontal: 16,
        marginTop: 16,
        elevation: 8,
        shadowColor: '#3E8DFB',
        marginBottom: 8,
    },

    inputStyle: {
        fontSize: 14,
        fontWeight: 'normal',
        color: '#000',
        //fontFamily: 'Montserrat-Regular',
    },
});

export default SearchBar;
