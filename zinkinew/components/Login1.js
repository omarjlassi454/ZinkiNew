import React, { useState } from 'react';
import { View, StyleSheet ,Image  } from 'react-native';
import IpAdress from './IpAdress';
import { useNavigation } from "@react-navigation/native";
import { RNKeycloak } from '@react-keycloak/native';
import { ReactNativeKeycloakProvider } from '@react-keycloak/native';
import SQLite from 'react-native-sqlite-storage';
import {Colors} from '../canstant/index'
import { TextInput, Button , Icon  } from 'react-native-paper';

const Login = () => {
  const [username, setusername] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();

  const handleLogin = async () => {
    // Perform login logic using Keycloak or any authentication mechanism
    try {
      // Make a request to your backend API to authenticate the user
      const response = await fetch(`${IpAdress.IP}/auth/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password }),
      });
      const responseData = await response.json();
      console.log('responseData :::::', responseData);

      if (response.ok) {
        console.log('TOKEN:', responseData.accessToken);
      
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
      
        // Retrieve user details from MongoDB and insert/update the single row in the TOKEN table
        db.transaction((tx) => {
          tx.executeSql(
            'CREATE TABLE IF NOT EXISTS TOKEN (token TEXT, UserId TEXT)',
            [],
            () => {
              // Table created or already exists
              tx.executeSql(
                'SELECT COUNT(*) FROM TOKEN',
                [],
                (_, resultSet) => {
                  const { rows } = resultSet;
                  const rowCount = rows.item(0)['COUNT(*)'];
                  if (rowCount === 0) {
                    // Insert a new row since there are no existing rows
                    tx.executeSql(
                      'INSERT INTO TOKEN (token, UserId) VALUES (?, ?)',
                      [responseData.accessToken, responseData.UserId],
                      (_, result) => {
                        if (result.rowsAffected > 0) {
                          console.log('Token and UserId inserted successfully');
                          // Additional code here if needed
                        } else {
                          console.log('No rows were affected');
                        }
                      },
                      (error) => {
                        console.log('Error inserting token:', error);
                      }
                    );
                  } else {
                    // Update the existing row
                    tx.executeSql(
                      'UPDATE TOKEN SET token = ?, UserId = ?',
                      [responseData.accessToken, responseData.UserId],
                      (_, result) => {
                        if (result.rowsAffected > 0) {
                          console.log('Token and UserId updated successfully');
                          // Additional code here if needed
                        } else {
                          console.log('No rows were affected');
                        }
                      },
                      (error) => {
                        console.log('Error updating token:', error);
                      }
                    );
                  }
                },
                (error) => {
                  console.log('Error counting rows in TOKEN:', error);
                }
              );
            },
            (error) => {
              console.log('Error creating table:', error);
            }
          );
        });
      
        // Login successful, retrieve user data from response if needed
        // Redirect to the home page or any other protected route
        navigation.replace('Home');
      }
      
       else {
        // Login failed, handle error (e.g., display an error message)
        console.error('Login failed');
      }
    } catch (error) {
      console.error('Error during login:', error);
    }
  };

  return (
    <View style={styles.container}>
       <Image
      source={require('../assets/logo1.png')}
      style={{ alignSelf: 'center', marginBottom:40 ,  width: '40%', height: '30%' }}
 
    />

       <View style={styles.inputContainer}>
      

<TextInput
        style={styles.input}
        placeholder="username"
        onChangeText={(text) => setusername(text)}
        right={ <TextInput.Icon
          icon={'account-outline'}
          iconColor="#6CA4FC"
      />}  

mode='outlined'
//keyboardType='account-address'
autoCapitalize='none'
autoCorrect={false}
theme={{
  colors: {
    primary: Colors.primary,
    background: 'white',
    text: 'black',      

  }
}}

        
      />
       </View>
      
       <View style={styles.inputContainer}>
      <TextInput
        style={styles.input}
        placeholder="Password"
        onChangeText={(text) => setPassword(text)}

        secureTextEntry
          right={<TextInput.Icon icon="lock"   iconColor="#6CA4FC"/>}   
          mode='outlined'
          autoCapitalize='none'
          autoCorrect={false}
          theme={{
            colors: {
              primary: Colors.primary,
              background: 'white',
              text: 'black'
            }
          }}
      />
      </View>
      <View style={styles.loginButtonSection}>
      <Button  mode="outlined"


style={{
    borderRadius: 20,
    marginVertical: 8,
    borderColor: "#6CA4FC",
}}
contentStyle={{ height: 40 }}
labelStyle={{ fontSize: 16 }}
textColor="#6CA4FC"  onPress={handleLogin} > Sign In</Button></View>
      <View >

<Button
  mode='text'
  onPress={() => navigation.navigate('SignUp')} 
labelStyle={{color: 'blue', fontSize: 14}}>
  Don't have an account ? Signup Here
</Button>
</View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    justifyContent: 'center',

  },
  inputContainer: {
    marginVertical: 8,
  },
  loginButtonSection: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
     marginTop: 50
 },
 textButtonSection: {
    
 },
 imageContainer: {
  position: 'absolute',
  top: 0,
  left: 0,
},
  input: {
    width: '100%',
    backgroundColor: 'white',
    
  },
  button: {
    marginVertical: 8,
    width: '75%',
    backgroundColor: Colors.primary,
    borderRadius: 15,




  },

});

export default Login;
