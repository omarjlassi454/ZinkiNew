import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Linking, Platform, TouchableOpacity, ActivityIndicator, TextInput, ScrollView, Modal, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import NumericInput from 'react-native-numeric-input'
import p1 from '../assets/images/p1.png';
import p2 from '../assets/images/p2.png';
import { CreditCardInput, LiteCreditCardInput } from 'react-native-credit-card-input';
import { Button } from 'react-native-paper';
import { WebView } from 'react-native-webview';
import { useNavigation } from '@react-navigation/native';
import NavBottom from './NavBottom'
import IpAdress from './IpAdress';
import SQLite from 'react-native-sqlite-storage';

const ProfileComponent = () => {
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    var [textFromDB, settextFromDB] = useState("");
    var [password, setPassword] = useState("");
    const [quotaNumber, setQuotaNumber] = useState(0);
    const [amount, setAmount] = useState(0);
    const [iduser, setiduser] = useState([]); // State to store the document IDs
    const [token, settoken] = useState([]); // State to store the document IDs
    const [quota, setQuota] = useState(null);
    const [loading, setLoading] = useState(true);
    const [user1, setuser1] = useState(null);
    const [loading1, setLoading1] = useState(true);
    const [User, setUser] = useState(null);


    const handleNumericInputChange = (value) => {
        const diff = value - quotaNumber;
        const amountChange = diff * 0.3;
        setAmount(amount + amountChange);
        setQuotaNumber(value);
    };
    const handleModalVisible = () => {
        setModalVisible(!modalVisible);
    };

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const handlePayment = () => {
        // Add your logic for handling the payment action
    };

    const handleLogout = () => {
        // Add your logic for handling the logout action
    };

    useEffect(() => {

    }, []);
    // console.log('Quota out side  ===> ', quota)
    useEffect(() => {
        // console.log('Quota here ===> ', quota);
    }, [quota]);


    // console.log('Quota out side ===> ', quota)


    useEffect(() => {


        console.log("UserUserUser1111111", User)

        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT * FROM TOKEN',
                [],
                (_, result) => {
                    const re = result.rows.raw();
                    console.log('UserId in home : ', re[0].UserId);
                    const userids = (re[0].UserId);
                    console.log('userid:in home ', userids);
                    setiduser(userids)
                    settoken(re[0].token)

                    console.log('111  length in home : ', re.length);
                    console.log('Retrieved rows  in home :', result.rows);

                    // Access the retrieved data
                    const tokens = result.rows.raw();
                    //console.log('Retrieved tokens:', tokens);

                    // Process and log each token
                    tokens.forEach((token, index) => {
                        //console.log(`Token ${index + 1}:`, token);
                    });
                },
                (_, error) => {
                    console.log('Error retrieving  tokens in home :', error);
                }
            );
        });

        const fetchQuota = async () => {
            try {
                setLoading(true); // Set loading to true before the fetch request

                const response = await fetch(`${IpAdress.IP}/GenerateSSML/quotas/${iduser}`);
                const data = await response.json();

                if (response.ok) {
                    const quotaData = data.quota;
                    setQuota(quotaData);
                } else {
                    console.log(data.error); // Log the error message
                }

            } catch (error) {
                console.error('Error:', error);
            } finally {
                setLoading(false); // Set loading to false after the fetch request, regardless of success or failure
            }
        };

        fetchQuota();

        const fetchuser1 = async () => {
            try {
                setLoading1(true); // Set loading to true before the fetch request

                const response = await fetch(`${IpAdress.IP}/GenerateSSML/user/${iduser}`);
                const data = await response.json();

                if (response.ok) {
                    const userdata = data.user;
                    setuser1(userdata);
                    console.log("userdata25 :", userdata)
                    console.log("datadatadata25 :", data)
                    console.log("user1user25 :", user1)
                } else {
                    console.log(data.error); // Log the error message
                }

            } catch (error) {
                console.error('Error:', error);
            } finally {
                setLoading1(false); // Set loading to false after the fetch request, regardless of success or failure
            }
        };
        console.log("user1user27 :", user1)

        fetchuser1();




        const handleDeepLink = async () => {
            // Get the initial URL when the app is launched through a deep link
            const initialUrl = await Linking.getInitialURL();

            if (initialUrl) {
                handleUrl(initialUrl); // Handle the deep link URL
            }

            // Listen for subsequent deep link URL changes
            Linking.addEventListener('url', handleUrl);

            return () => {
                // Clean up the event listener when the component is unmounted
                Linking.removeEventListener('url', handleUrl);
            };
        };

        const handleUrl = (url) => {
            // Handle the deep link URL here
            console.log('Deep link URL:', url);
            // Implement the necessary actions, such as navigating to a specific screen or executing a function
        };

        // Call the handleDeepLink function when the component mounts
        handleDeepLink();
    }, [iduser]);

    const handleNextButtonClick = async () => {
        const apiUrl = 'https://sandbox.paymee.tn/api/v2/payments/create';
        const requestData = {
            amount: amount,
            note: 'Order #123',
            first_name: user1.firstName,
            last_name: user1.lastName,
            email: user1.email,
            phone: user1.mobileNumber,
            return_url: 'https://www.10rowsaday.com/paypal-payment',
            cancel_url: 'https://www.paymee.tn',
            webhook_url: 'https://www.paymee.tn',
            order_id: '244557'
        };

        try {
            const response = await fetch(apiUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Token 8477227817b179b871c0ab0ae685fb2151dd920b' // Replace 'api_key' with your actual API key
                },
                body: JSON.stringify(requestData)
            });

            const responseData = await response.json();

            // Check if the response is successful and has the expected data structure
            if (response.ok && responseData && responseData.status && responseData.data && responseData.data.token) {
                const token = responseData.data.token;
                settoken(token);
                // Use the token for further processing

                console.log('Token:', token);
                navigation.navigate('Payment', { token })

                const response2 = await fetch(`${IpAdress.IP}/GenerateSSML/Updatequotamaxvalue/`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        id: iduser,
                        number: quotaNumber
                    }),
                });

                if (response2.ok) {
                    console.log('payment success and quota number updated', quotaNumber, iduser);
                    setAmount(0);
                    setQuotaNumber(0);
                    const fetchQuota = async () => {
                        try {
                            const response = await fetch(`${IpAdress.IP}/GenerateSSML/quotas/${iduser}`);
                            const data = await response.json();

                            if (response.ok) {
                                const quotaData = data.quota;
                                setQuota(quotaData);
                            } else {
                                console.log(data.error);
                            }
                        } catch (error) {
                            console.error('Error:', error);
                        }
                    };

                    fetchQuota();
                }
            } else {
                console.error('Invalid response data:', responseData);
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };
    //console.log('paymee screen shows up from profile');

    return (



        <View style={styles.container}>

            <ScrollView>
                <View style={styles.card}>
                    {loading1 ? (
                        <Text>Loading...</Text>
                    ) : user1 ? (
                        <>
                            <Text style={styles.cardTitle}>Email</Text>
                            <TextInput
                                editable={false}
                                style={[styles.input, styles.emailInput]}
                                placeholder="Enter your email"
                                value={user1.email} // Access the email property from user1 object
                            />

                            <Text style={styles.cardTitle}>Password</Text>
                            <View style={styles.passwordContainer}>
                                <TextInput
                                    editable={false}
                                    style={[styles.input, styles.passwordInput]}
                                    placeholder="Enter your password"
                                    secureTextEntry={!showPassword}
                                    value={user1.password}
                                />
                                <TouchableOpacity onPress={togglePasswordVisibility} style={styles.eyeIconContainer}>
                                    <Icon name={showPassword ? 'eye-slash' : 'eye'} size={20} color="#6CA4FC" />
                                </TouchableOpacity>
                            </View>
                        </>
                    ) : (
                        <Text>Quota data not available.</Text>
                    )}
                </View>




                <View style={styles.card}>

                   


                    {loading ? (
                        <Text>Loading...</Text>
                    ) : quota ? (
                        <Text style={styles.cardTitle}>Transcription Quota per image : {quota.quota_number}/{quota.max_value}</Text>
                    ) : (
                        <Text>Quota data not available.</Text>
                    )}
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 10,
                    }}>
                        <Button
                            mode="outlined"
                            onPress={() => {
                                setModalVisible(true);


                            }}

                            style={{
                                borderRadius: 20,
                                marginVertical: 8,
                                borderColor: "#6CA4FC",
                            }}
                            contentStyle={{ height: 40 }}
                            labelStyle={{ fontSize: 16 }}
                            textColor="#6CA4FC"
                        >
                            Add quota
                        </Button>
                    </View>
                </View>

                <View style={styles.card}>
                    <Text style={styles.cardTitle}>Speech Quota per character : 56964/4000000 </Text>
                    {/* Add your logic and UI elements for speech quota */}
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 10,
                    }}>
                        <Button
                            mode="outlined"

                            onPress={() => {
                                setModalVisible(true);


                            }}
                            style={{
                                borderRadius: 20,
                                marginVertical: 8,
                                borderColor: "#6CA4FC",
                            }}
                            contentStyle={{ height: 40 }}
                            labelStyle={{ fontSize: 16 }}
                            textColor="#6CA4FC"
                        >
                            Add quota
                        </Button>
                    </View>
                </View>

                <View style={styles.card}>
                    <TouchableOpacity onPress={handleLogout} style={styles.logoutButton}>
                        <Icon name="sign-out" size={20} color="#6CA4FC" />
                        <Text style={styles.logoutButtonText}>Logout</Text>
                    </TouchableOpacity>
                </View>


            </ScrollView>



            <Modal transparent={true} visible={modalVisible} animationType="slide" >
                <View style={styles.previewModalContainer}>
                    <TouchableOpacity onPress={() => {
                        setModalVisible(false);


                    }}>
                        <View style={styles.separatorLine}>
                            {/* Add content for the separator line here */}
                        </View>
                    </TouchableOpacity>
                    <ProgressSteps
                        activeStepIconBorderColor="#6CA4FC"
                        completedStepIconColor="#6CA4FC"
                        completedProgressBarColor="#6CA4FC"
                        activeLabelColor="#6CA4FC"
                        activeStepNumColor="#2B3270"
                        completedStepNumColor="#6CA4FC" >

                        <ProgressStep
                            label="Quota number"
                            nextBtnText="Next"
                            previousBtnText="Previous"
                            nextBtnTextStyle={styles.stepText}
                            previousBtnTextStyle={styles.stepText}
                            nextBtnStyle={styles.nextButton}
                            previousBtnStyle={styles.previousButton}
                        >
                            <ScrollView>
                                <View style={styles.stepContainer}>
                                    <Text style={styles.stepLabel}>Update quota number for document transcription</Text>
                                    <View style={styles.numericInputContainer}>
                                        <NumericInput
                                            type="plus-minus"
                                            minValue={0}
                                            maxValue={1000}
                                            step={1}
                                            valueType="integer"
                                            value={quotaNumber}
                                            onChange={handleNumericInputChange}
                                            rounded
                                            totalWidth={200}
                                            totalHeight={50}
                                            textColor="#000"
                                            iconStyle={{ color: '#000' }}
                                            rightButtonBackgroundColor="#e6e6e6"
                                            leftButtonBackgroundColor="#e6e6e6"
                                        />
                                    </View>
                                    <View style={styles.updatedAmountContainer}>
                                        <Text style={styles.updatedAmountText}>Total amount : {amount} DT</Text>
                                    </View>
                                </View>
                            </ScrollView>
                        </ProgressStep>





                        <ProgressStep
                            label="Payment Information"
                            finishBtnText="Pay"
                            previousBtnText="Previous"
                            nextBtnTextStyle={styles.stepText}
                            previousBtnTextStyle={styles.stepText}
                            nextBtnStyle={styles.nextButton}
                            previousBtnStyle={styles.previousButton}
                            onSubmit={handleNextButtonClick}
                        >
                            <View style={styles.stepContainer}>
                                <Text style={styles.stepLabel}>Enter credit card number</Text>

                                <View style={styles.paymentFieldContainer}>
                                    <LiteCreditCardInput
                                        validColor={'#6CA4FC'}
                                        invalidColor={'black'}
                                        // onChange={handleCardNumberChange}
                                        placeholder="Enter card number"
                                        keyboardType="numeric"
                                        inputStyle={styles.inputtt}
                                        containerStyle={styles.inputContainer}

                                    />
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 50, }}>
                                    <Image source={p2} style={{ width: 220, height: 40 }} />
                                    <Text style={{ color: '#BFBFBF', fontSize: 12, textAlign: 'center' }}>Payment by credit card is managed by company Monétique Tunisie</Text>
                                </View>

                            </View>
                        </ProgressStep>



                    </ProgressSteps>
                </View>
                {isLoading && (
                    <View style={styles.loadingIndicator}>
                        <ActivityIndicator size="large" color="#6CA4FC" />
                    </View>
                )}
            </Modal>






            <NavBottom></NavBottom>

        </View>

    );
};

const styles = StyleSheet.create({

    emailInput: {
        color: '#000',
        width: 316,// Modify the color for the email input
    },

    passwordInput: {
        color: '#000', // Modify the color for the password input
    },

    container: {
        flex: 1,
        padding: 16,
    },
    card: {
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 16,
        marginBottom: 16,

    },
    cardTitle: {
        fontSize: 16,
        fontWeight: 'normal',
        marginBottom: 8,
        color: '#2B3270'
    },

    passwordContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    eyeIconContainer: {
        marginLeft: 8,
    },
    paymentButton: {
        backgroundColor: '#6CA4FC',
        borderRadius: 4,
        padding: 12,
        width: 140,
        alignItems: 'center',
        marginTop: 8,
        justifyContent: 'center',
    },
    paymentButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
    },
    logoutButton: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logoutButtonText: {
        color: '#888',
        marginLeft: 8,
        fontSize: 16,
    },
    previewModalContainer: {
        position: 'absolute',
        bottom: 0,
        left: 5,
        right: 5,
        height: '85%',
        backgroundColor: '#FFFFFF',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingHorizontal: 20,
        paddingVertical: 30,
        zIndex: 0,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4,
        elevation: 30,
    },

    separatorLine: {
        height: 3,
        width: '30%',
        backgroundColor: '#CCD8EE',
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    nextButton: {
        backgroundColor: 'transparent',
        borderColor: '#6CA4FC',
        // Change next button background color
    },
    previousButton: {
        backgroundColor: 'transparent',
        borderColor: '#6CA4FC',
    },
    stepText: {
        fontSize: 15,
        fontWeight: 'normal',
        color: '#6CA4FC'
    },
    buttonTextStyle: {
        color: '#6CA4FC', // Change button text color
    },
    documentTypeContainer: {
        marginTop: 20,
        justifyContent: 'space-between',
    },

    documentTypeItem: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },

    documentTypeText: {
        fontSize: 16,
        marginRight: 10,
    },

    stepContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
        marginTop: 10,
    },
    stepLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#2B3270'
    },
    numericInputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
    },
    updatedAmountText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#2B3270'
    },
    updatedAmountContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2, // Add border width
        borderColor: '#6CA4FC', // Add border color
        padding: 10,
        borderRadius: 30,
        marginTop: 35,
    },
    paymentFieldContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 30,
    },





    fieldLabel: {
        width: 100,
        marginRight: 10,
        fontSize: 16,
    },
    input: {
        flex: 1,
        height: 40,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 5,
        paddingLeft: 10,

    },
    inputContainer: {
        borderBottomWidth: 1,
        borderBottomColor: 'blue',
    },
    inputtt: {
        paddingBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#2B3270',
        width: 180
    },
});

export default ProfileComponent;
