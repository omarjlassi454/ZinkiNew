import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Modal, ScrollView, ActivityIndicator, Image,SafeAreaView, Alert, Platform, PermissionsAndroid, ScrollViewBase, NativeModules, ToastAndroid} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SQLite from 'react-native-sqlite-storage';
import IpAdress from './IpAdress';
import AppBar from './AppBar';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import noDataImage from '../assets/images/noData.png';
import noDataImage2 from '../assets/images/img4.png';
import { TextInput, Button, Menu, Divider, Provider,  RadioButton, } from 'react-native-paper';
import RNFS from 'react-native-fs';
import { useNavigation } from "@react-navigation/native";

//import History from './History';


import {
  launchCamera,
  launchImageLibrary,
} from 'react-native-image-picker';
import SearchBar from './SearchBar';
import NavBottom from './NavBottom'

const Home = () => {
  // const navigation = useNavigation();
  const db = SQLite.openDatabase('mydb.db');
  const [modalVisible, setModalVisible] = useState(false);
  const [filePath, setFilePath] = useState({});
  const [transcribeImage, setTranscribeImage] = useState('');
  const [imageUriFromDB, setImageUriFromDB] = useState(null);
  const [baseImage, setBaseImage] = useState('');
  const [imageDatabaseUri, setImageDatabaseUri] = useState(null);
  const [imageType, setImageType] = useState('');
  const [images, setImages] = useState([]);
  const [imageURIs, setImageURIs] = useState([]);
  const [name, setName] = useState('');
  const [uploadDate, setUploadDate] = useState('');
  const [text, setText] = useState('');
  const navigation = useNavigation();
  const [names, setNames] = useState('');
  const [dates, setDates] = useState('');
  const [types, setTypes] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [iduser, setiduser] = useState([]); // State to store the document IDs
  const [token, settoken] = useState([]); // State to store the document IDs
  const [DocId, setDocId] = useState([]); // State to store the document IDs
  
  useEffect(() => {


    const createDocsTableSql = `
CREATE TABLE IF NOT EXISTS DOCS (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  URI TEXT,
  Text TEXT,
  Type TEXT,
  Name Text,
  Date Date,
  Status Boolean,
  DefaultSSMl TEXT,
  UserId INTEGER,
  Url TEXT,
  FOREIGN KEY (UserId) REFERENCES users(id)
);
`;

    const createTableSql = `
  CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    email TEXT,
    tel TEXT,
    password TEXT
  );
`;
    const insertUserSql = `
  INSERT INTO users (name, email, tel, password)
  VALUES (?, ?, ?, ?);
`;


    const insertDocsSql = `
  INSERT INTO DOCS (URI,Text, Type, Name, Date, Status, DefaultSSMl, UserId, Url)
  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);
`;
    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });


    db.transaction((tx) => {
      tx.executeSql(createTableSql, [], (_, result) => {
        console.log('Table created successfully');
      }, (_, error) => {
        console.log('Error creating table:', error);
      });
    });

    // Create the "Docs" table if it doesn't already exist

    db.transaction((tx) => {
      tx.executeSql(createDocsTableSql, [], (_, result) => {
        console.log('Table Docs created successfully');
      }, (_, error) => {
        console.log('Error creating Docs  table:', error);
      });
    });

    /*db.transaction((tx) => {
      tx.executeSql(insertDocsSql, ['http//:', ' To be, or not to be, that is the question Whether tis nobler in the mind to suffer The slings and arrows of outrageous fortune, Or to take arms against a sea of troubles And by opposing end them', imageType, '<speak>To be, or not to be, that is the question Whether tis nobler in the mind to suffer The slings and arrows of outrageous fortune,Or to take arms against a sea of troubles And by opposing end them </speak>', 33, `${IpAdress.IP}/audio`], (_, result) => {
        console.log('User inserted successfully');
      }, (_, error) => {
        console.log('Error inserting Docs:', error);
      });
    });*/


    /*
        // Execute a DELETE query on Docs
        db.transaction((tx) => {
          const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
          tx.executeSql(
            'DELETE FROM Docs ',
            [],
            (tx, result) => {
              console.log('Data deleted successfully');
            },
            (error) => {
              console.log('Error executing SQL statement:', error);
            }
          );
        });
        /*
        db.transaction((tx) => {
          tx.executeSql('DROP TABLE IF EXISTS DOCS', [], (_, result) => {
            console.log('DOCS table deleted successfully.');
          },
            (_, error) => {
              console.log('Error deleting DOCS table:', error);
            });
        });
        */

  }, []);

  useEffect(() => {
    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

  
    }, []);

    console.log('iduseriduser in home : ', iduser);

 
useEffect(() => {

  const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
  db.transaction((tx) => {
    tx.executeSql(
      'SELECT * FROM TOKEN',
      [],
      (_, result) => {
        const re = result.rows.raw();
        console.log('UserId in home : ',  re[0].UserId);
        const userids =(re[0].UserId);
        console.log('userid:in home ',  userids);
        setiduser(userids)
        settoken( re[0].token)

        console.log('111  length in home : ', re.length);
        console.log('Retrieved rows  in home :', result.rows);
  
        // Access the retrieved data
        const tokens = result.rows.raw();
       //console.log('Retrieved tokens:', tokens);
        
        // Process and log each token
        tokens.forEach((token, index) => {
          //console.log(`Token ${index + 1}:`, token);
        });
      },
      (_, error) => {
        console.log('Error retrieving  tokens in home :', error);
      }
    );
  });

  db.transaction((tx) => {
    tx.executeSql(
      'SELECT id , URI, Name, Date, Type FROM DOCS WHERE UserId = ? ORDER BY id DESC LIMIT 3',
      [iduser],
      (_, result) => {
        const imageURIs = [];
        const names = [];
        const dates = [];
        const types = [];
        const DocId = []
        for (let i = 0; i < result.rows.length; i++) {
          const row = result.rows.item(i);
          names.push(row.Name);
          dates.push(row.Date);
          types.push(row.Type);
          imageURIs.push(row.URI);
          DocId.push(row.id);

        }
        setNames(names);
        setDates(dates);
        setTypes(types);
        setImageURIs(imageURIs);
        setDocId(DocId);

      },
      (_, error) => {
        console.log('Error retrieving images:', error);
      }
    );
  });
}, [imageDatabaseUri, iduser]);





  const onNextStep = () => {
    // Handle logic when moving to the next step
  };

  const onPrevStep = () => {
    // Handle logic when moving to the previous step
  };
  const handleModalVisible = () => {
    setModalVisible(!modalVisible);
  };

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else { return true; }
  };


  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const permissions = [
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        ];
        const granted = await PermissionsAndroid.requestMultiple(permissions);
        // If WRITE_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions are granted
        return (
          granted[PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE] ===
          PermissionsAndroid.RESULTS.GRANTED &&
          granted[PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE] ===
          PermissionsAndroid.RESULTS.GRANTED
        );
      } catch (err) {
        console.warn(err);
        alert('Permission request error', err);
      }
      return false;
    } else {
      return true;
    }
  };

  const captureImage = async (type) => {

    let options = {
      mediaType: 'photo',
      quality: 1,
      saveToPhotos: true,
      includeBase64: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();

    if (isCameraPermitted && isStoragePermitted) {


      launchCamera(options, (response) => {

        console.log('Response = ', response);
        if (response.didCancel) {
          console.log('Image picker cancelled');
          // Set imageUri to an empty value
          setImageUriFromDB(null);
          return;
        } else if (response.errorCode == 'camera_unavailable') {
          alert('Camera not available on device');
          return;
        } else if (response.errorCode == 'permission') {
          alert('Permission not satisfied');
          return;
        } else if (response.errorCode == 'others') {
          alert(response.errorMessage);
          return;
        }

        const imageUri = response.assets[0].uri;

        const baseString = response.assets[0].base64;
        setBaseImage(baseString);
        const randomNumber = Math.floor(1000 + Math.random() * 9000);
        const fileName = `Image${randomNumber}.jpg`;
        setName(fileName);
        setImageDatabaseUri(imageUri);
        console.log('imageUri -> ', imageUri);
        const uploadDate = new Date();
        const formattedDate = `${uploadDate.toISOString().split('T')[0]} ${uploadDate.getHours()}:${uploadDate.getMinutes()}`;
        setUploadDate(formattedDate);
        console.log('formattedDate -> ', formattedDate);


        db.transaction((tx) => {
          tx.executeSql(
            'SELECT * FROM DOCS ',
            [],
            (tx, results) => {
              const DOCS = results.rows.raw();
              console.log('DOCS here --->:', DOCS);
            },
            (error) => {
              console.log('Error executing SQL statement:', error);
            }
          );
        });

        console.log('image uri nowww ----->', imageUriFromDB)
        console.log('base64 ->', response.assets[0].base64, 'base base');
        console.log('uri -> ', response.assets[0].uri);
        console.log('width -> ', response.assets[0].width);
        console.log('height -> ', response.assets[0].height);
        console.log('fileSize -> ', response.assets[0].fileSize);
        console.log('type -> ', response.assets[0].type);
        console.log('fileName -> ', response.assets[0].fileName);
        setFilePath(response);
      });
    }
  };

  const chooseFile = (type) => {
    let options = {
      mediaType: "photo",
      quality: 1,
      saveToPhotos: true,
      includeBase64: true,
    };
    launchImageLibrary(options, (response) => {


      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('Image picker  cancelled');
        // Set imageUri to an empty value
        setImageUriFromDB(null);
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      const imageUri = response.assets[0].uri;
      setImageDatabaseUri(imageUri);
      const baseString = response.assets[0].base64;
      setBaseImage(baseString);
      const randomNumber = Math.floor(Math.random() * 10000);
      const fileName = `Image${randomNumber}.jpg`;
      setName(fileName);
      console.log('imageUri -> ', imageUri);
      const uploadDate = new Date();
      const formattedDate = `${uploadDate.toISOString().split('T')[0]} ${uploadDate.getHours()}:${uploadDate.getMinutes()}`;
      setUploadDate(formattedDate);
      console.log('formattedDate -> ', formattedDate);

      console.log('base64 -> ', response.assets[0].base64);
      console.log('uri -> ', response.assets[0].uri);
      console.log('width -> ', response.assets[0].width);
      console.log('height -> ', response.assets[0].height);
      console.log('fileSize -> ', response.assets[0].fileSize);
      console.log('type -> ', response.assets[0].type);
      console.log('fileName -> ', response.assets[0].fileName);
      setFilePath(response);
      const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

      db.transaction((tx) => {
        tx.executeSql(
          'SELECT * FROM DOCS  ',
          [],
          (tx, results) => {
            const DOCS = results.rows.raw();
            console.log('DOCS here --->:', DOCS);
          },
          (error) => {
            console.log('Error executing SQL statement:', error);
          }
        );
      });
    });

  };
  console.log('image type here -->', imageType)
  const callApiForOldPrint = async () => {
    setIsLoading(true)
    try {
      const response = await fetch(`${IpAdress.IP}/GenerateSSML/TesteOCR/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          encodedImage: baseImage, id:iduser
        }),
      });

      if (!response.ok) {
        throw new Error('OCR request failed');
      }

      const data = await response.json();

      if (
        data &&
        Array.isArray(data.paragraphs) &&
        data.paragraphs.length > 0
      ) {
        const { paragraphs } = data;
        const OCRtext = paragraphs.join('\n');
        console.log('text state before storing:', OCRtext);
        await storeImageUriInDatabase(OCRtext);
        setText('');
        resetFormFields();
      } else {
        throw new Error('Invalid OCR response');
      }
    } catch (error) {
      console.error('Error during OCR request:', error);
    } finally {
      setIsLoading(false);
    }
  };

  const callApiForSpecialImageTypes = async () => {
    try {
      const response1 = await fetch(`${IpAdress.IP}/GenerateSSML/QuotaDocs/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: iduser,
        }),
      });
      
      const responseData = await response1.json();
      console.log("response1.message", responseData.message);
  
      if (responseData.message === 'you must charge your account') {
        console.log(responseData.message);
        Alert.alert(
          "Quota expired",
          "you can add more quota below here.",
          [
            {
              text: "Add",
              onPress: () => navigation.navigate('Profile', {
                id: iduser,
              })
            },
            {
              text: "Cancel",
              style: "cancel"
            }
          ]
        );
      } else if (responseData.message === 'quota checked') {
        setIsLoading(true);
  
        const response = await fetch('https://cfpe2jl53jd2ntwuxa5d2fgp6m0gzevq.lambda-url.eu-central-1.on.aws/function/invocations', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            image: baseImage,
            script: imageType,
          }),
        });
  
        if (!response.ok) {
          throw new Error('OCR request failed');
        }
  
        const response2 = await fetch(`${IpAdress.IP}/GenerateSSML/Updatequota/`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            id: iduser,
          }),
        });
  
        if (response2.ok) {
          ToastAndroid.show('document successfully transcribed', ToastAndroid.SHORT);
        }
  
        const data = await response.json();
        console.log("datadata111" , data)
        await storeImageUriInDatabase(data.text);
        setText('');
        resetFormFields();
      }
    } catch (error) {
      console.error('Error during OCR request:', error);
    } finally {
      setIsLoading(false);
    }
  };
  

  const handleTranscribe = async () => {
    if (imageDatabaseUri === null) {
      Alert.alert(
        'Warning',
        'Please select an image first',
        [{ text: 'OK' }],
        { cancelable: false }
      );
      return;
    }

    try {
      if (imageType === 'Other') {
        await callApiForOldPrint();
      } 
      else if (imageType === 'nasikh' || imageType === 'maghribi' || imageType === 'oldprint'){

        await callApiForSpecialImageTypes();
      }
      
      else {
        // Handle other image types here
        console.log('Image type is not oldprint');
        // Additional logic if needed
      }
    } catch (error) {
      console.error('Error during OCR request:', error);
    } 
  };

  const storeImageUriInDatabase = async (Text) => {

    const text = Text.trim().replace(/[\s\n]+/g, ' ');

    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
    await new Promise((resolve, reject) => {
      db.transaction((tx) => {
        const query =
          'INSERT INTO DOCS (URI, Text, Type, Name, Date, Status, DefaultSSMl, UserId, Url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);';
        const values = [
          imageDatabaseUri,
          text,
          imageType,
          name,
          uploadDate,
          false,
          'hhh',
          iduser,
          'http//:',
        ];
        console.log('SQL query:', query);
        console.log('Values:', values);
        tx.executeSql(
          query,
          values,
          (_, result) => {
            const insertedId = result.insertId;
            console.log('Image URI stored successfully in the "DOCS" table. documentId:', insertedId);
            resolve(insertedId);
            console.log('Image URI  stored successfully in    the "DOCS" table. documentId ' );
            navigation.replace('FixText', { documentId: insertedId, userId: iduser });
          },
          (_, error) => {
            console.log('Error storing image URI:', error);
            reject(error);
          }
        );
      });
    });

    
   
  };

  const resetFormFields = () => {
    setImageUriFromDB(null);
    setImageDatabaseUri(null);
    setModalVisible(false);
    setImageType('');
    setName('');
    setUploadDate('');
    setBaseImage('');
  };


  useEffect(() => {
    console.log('text dstate ==>', text);
  }, [text]);


  console.log ("iduser----1", iduser)

  return (




    <View style={styles.container}>
      <View style={styles.bannerContainer}>
       
      </View>

      <View style={{ marginTop:20 }}>
        <View style={{ paddingHorizontal: 20, paddingBottom: 5 }}>
          <Text style={{ fontSize: 16, fontWeight: 'normal', color: '#2B3270' }}>
            Recent files
          </Text>
          <View style={{ borderBottomWidth: 1.5, width: '34%', borderBottomColor: '#6CA4FC', marginTop: 6, marginBottom: 10 }} />
        </View>
        <View style={{ marginBottom: 1}}>
          {imageURIs.length === 0 ? (
            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 150 }}>
              <Image source={noDataImage2} style={{ width: 160, height: 160 }} />
              <Text style={{ color: '#BFBFBF', fontSize: 12, textAlign: 'center' }}>There are no recent documents to display yet</Text>
            </View>
          ) : (
              <ScrollView vertical showsVerticalScrollIndicator={false}>
                <View style={styles.wrapContainer}>
                  {imageURIs.map((uri, index) => (

                      <View key={index} style={styles.cardContainer}>
                        <View style={styles.card}>
                          <Image source={{ uri }} style={styles.images} />
                          <View style={styles.details}>
                          <Text style={styles.date}>{names[index]}</Text>
                            <Text style={styles.name}>{types[index]} document</Text>
                            <Text style={styles.name}>{dates[index]}</Text>
                          </View>
                          <View style={styles.iconContainer}>
                            <TouchableOpacity onPress={() => navigation.navigate('TextToSpeech', {
                  documentId: DocId[index]
                })} >
                            <Icon name="mic" size={22} color="#2B3270" />
                            </TouchableOpacity>
                          </View>
                        </View>
                        
                      </View>
                  
                  ))}
                </View>
              </ScrollView>

          )}
        </View>
        
      </View>

      <TouchableOpacity style={styles.addButton} onPress={handleModalVisible}>
        <Text style={styles.addButtonText}>+</Text>
      </TouchableOpacity>








     
      <Modal transparent={true} visible={modalVisible} animationType="slide" >
        <View style={styles.previewModalContainer}>
          <TouchableOpacity onPress={() => {
            setImageUriFromDB(null);
            setModalVisible(false);
            setImageType('');
            setImageDatabaseUri(null);
            setName('');
            setUploadDate('');
            setBaseImage('')
            setText('');

          }}>
            <View style={styles.separatorLine}>
              {/* Add content for the separator line here */}
            </View>
          </TouchableOpacity>
          <ProgressSteps
            activeStepIconBorderColor="#6CA4FC"
            completedStepIconColor="#6CA4FC"
            completedProgressBarColor="#6CA4FC"
            activeLabelColor="#6CA4FC"
            activeStepNumColor="#2B3270"
            completedStepNumColor="#6CA4FC" >

            <ProgressStep
              label="Document Type"
              onNext={onNextStep}
              onPrevious={onPrevStep}
              nextBtnText="Next"
              previousBtnText="Previous"
              nextBtnTextStyle={styles.stepText}
              previousBtnTextStyle={styles.stepText}
              nextBtnStyle={styles.nextButton}
              previousBtnStyle={styles.previousButton}


            >
              <View>
                <Text style={styles.stepLabel}>Choose a document type </Text>
                <TouchableOpacity
                  style={[styles.checkboxButton, imageType === 'nasikh' && styles.checkboxButtonSelected]}
                  onPress={() => {
                    setImageType(imageType === 'nasikh' ? '' : 'nasikh');
                  }}
                >
                  <Text style={styles.apllyButtonText}>Mashriqui manuscript</Text>

                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.checkboxButton, imageType === 'maghribi' && styles.checkboxButtonSelected]}
                  onPress={() => {
                    setImageType(imageType === 'maghribi' ? '' : 'maghribi');
                  }}
                >
                  <Text style={styles.apllyButtonText}>Maghribi manuscript</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.checkboxButton, imageType === 'oldprint' && styles.checkboxButtonSelected]}
                  onPress={() => {
                    setImageType(imageType === 'oldprint' ? '' : 'oldprint');
                  }}
                >
                  <Text style={styles.apllyButtonText}>Old print</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.checkboxButton, imageType === 'Other' && styles.checkboxButtonSelected]}
                  onPress={() => {
                    setImageType(imageType === 'Other' ? '' : 'Other');
                  }}
                >
                  <Text style={styles.apllyButtonText}>Other</Text>
                </TouchableOpacity>
              </View>
            </ProgressStep>


            <ProgressStep
              label="Image Source"
              onNext={onNextStep}
              onPrevious={onPrevStep}
              nextBtnText="Next"
              previousBtnText="Previous"
              nextBtnTextStyle={styles.stepText}
              previousBtnTextStyle={styles.stepText}
              nextBtnStyle={styles.nextButton}
              previousBtnStyle={styles.previousButton}
            >
              <View style={styles.stepContainer}>
                <Text style={styles.stepLabel}>Choose an image source</Text>

                <View style={styles.imageSourceContainer}>
                  <TouchableOpacity style={styles.storageOption} onPress={() => {
                    if (imageType === '') {
                      Alert.alert(
                        'Warning',
                        'Please select a document type first',
                        [{ text: 'OK' }],
                        { cancelable: false, }
                      );
                    } else {
                      captureImage('photo');
                    }
                  }}>
                    <Icon name="camera-outline" size={45} color="#2B3270" />
                    <Text style={styles.iconLabel}>Camera</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.storageOption} onPress={() => {
                    if (imageType === '') {
                      Alert.alert(
                        'Warning',
                        'Please select a document type first',
                        [{ text: 'OK' }],
                        { cancelable: false, }
                      );
                    } else { chooseFile('photo') }
                  }}>
                    <Icon name="image-outline" size={45} color="#2B3270" />
                    <Text style={styles.iconLabel}>Gallery</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ProgressStep>

            <ProgressStep
              label="Transcribe Image"
              onNext={onNextStep}
              onPrevious={onPrevStep}
              finishBtnText="Transcribe"
              previousBtnText="Previous"
              nextBtnTextStyle={styles.stepText}
              previousBtnTextStyle={styles.stepText}
              nextBtnStyle={styles.nextButton}
              previousBtnStyle={styles.previousButton}
              onSubmit={() => handleTranscribe()}
            >
              <View>

                <View style={styles.imageContainer}>
                  {imageDatabaseUri ? (
                    <Image source={{ uri: imageDatabaseUri }} style={styles.image} />
                  ) : (
                    <View style={styles.noDataContainer}>
                      <Image source={noDataImage} style={{ width: 160, height: 160 }} />
                      <Text style={{ color: '#BFBFBF', fontSize: 12 }}>No image yet</Text>
                    </View>
                  )}
                </View>
              </View>
            </ProgressStep>
          </ProgressSteps>
        </View>
        {isLoading && (
          <View style={styles.loadingIndicator}>
            <ActivityIndicator size="large" color="#6CA4FC" />
          </View>
        )}
      </Modal>
    
     
     
      <NavBottom></NavBottom>
    </View>
  );


};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  addButton: {
    zIndex: 10,
    position: 'absolute',
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#2B3270',
    alignItems: 'center',
    justifyContent: 'center',
    //right: 20,
    bottom: 75,
    elevation: 8,
    left: 330,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  closeButton: {
    position: 'absolute',
    top: 20,
    right: 20,
  },
  contentContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  iconButton: {
    alignItems: 'center',
    marginBottom: 20,
  },
  iconText: {
    marginTop: 10,
    fontSize: 18,
  },

  modalContent: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: -20,
    color: 'gray'

  },

  modalButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20
  },
  modalButton: {
    backgroundColor: '#2196F3',
    borderRadius: 10,
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginLeft: 10,
    justifyContent: 'center'
  },
  modalButtonText: {
    color: 'white',
    fontSize: 14,
  },
  modalCloseButton: {
    top: -10,
    left: 230,
  },
  searchContainer: {
    flex: 0,
    zIndex: 1,
  },
  iconsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  iconCard: {
    width: '25%',
    height: 120,
    borderRadius: 8,
    backgroundColor: '#FFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 20,
    elevation: 3,
    marginBottom: 16,
  },
  icon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconLabel: {
    marginTop: 8,
    marginRight: 8,

    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#2B3270',
  },

  storageOption: {
    alignItems: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },

  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    top: -30,
    left: 60,
    shadowOpacity: 0.25,

  },
  buttonClose2: {
    top: -30,
    left: 85,
    shadowOpacity: 0.25,

  },
  buttonClose3: {
    top: -30,
    left: 40,
    shadowOpacity: 0.25,

  },
 
  cardContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    marginLeft: 10,
  },
  transfertImage: {
    width: 100,
    height: 100,
  },
  cardText: {
    color: '#000',
    fontSize: 15,
    fontWeight: 'bold',
  },
  cardSubtitle: {
    color: '#555',
    fontSize: 16,
    marginBottom: 10,
  },
  cardHeaderText: {
    fontSize: 12,
    marginBottom: 10,
    left: 110,
  },

  textStyle: {
    color: 'grey',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  previewModalContainer: {
    position: 'absolute',
    bottom: 0,
    left: 5,
    right: 5,
    height: '85%',
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 30,
    zIndex: 30,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 30,
  },

  separatorLine: {
    height: 3,
    width: '30%',
    backgroundColor: '#CCD8EE',
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  checkboxButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    borderRadius: 15,
    height: 40,
    width: 200,
    paddingHorizontal: 8,

  },
  checkboxButtonSelected: {
    borderWidth: 1,
    borderColor: '#6CA4FC',
    //backgroundColor: '#6CA4FC'
  },
  apllyButtonText: {
    color: '#2B3270',
    fontSize: 14,
    fontWeight: 'bold',
  },
  imageSourceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 70,
  },
  stepLabel: {
    fontSize: 14,
    fontWeight: 'normal',
    marginTop: 15,
  },

  stepText: {
    fontSize: 15,
    fontWeight: 'normal',
    color: '#6CA4FC'
  },
  buttonTextStyle: {
    color: '#6CA4FC', // Change button text color
  },
  nextButton: {
    backgroundColor: 'transparent',
    borderColor: '#6CA4FC',
    // Change next button background color
  },
  previousButton: {
    backgroundColor: 'transparent',
    borderColor: '#6CA4FC',
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 200,
    height: 200,
    borderTopLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderWidth: 5,
    borderColor: '#CCD8EE',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 3,
    shadowRadius: 2,
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  noDataContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
 
  images: {
    width: 110,
    height: 130,
    margin: 5,
    marginBottom: 10,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    backgroundColor: '#888',
    opacity: 0.8,
  },
  cardHome: {
    width: '30%',
    height: 120,
    backgroundColor: 'transparent',
    margin: 5,
    //elevation: 0, // Remove shadow
    borderWidth: 0, // Remove border
    shadowColor: '#CCD8EE',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  containerPlus: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ECECEC',
    backgroundColor: '#F6F6F6',
    borderTopWidth: 1,
    borderRadius: 50,
    width: 300,
    height: 45,
    marginLeft: 30,
    marginRight: 10,
    marginTop: 20,
  },
  touchableOpacity: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    marginLeft: 5,
    color: '#2B3270',
  },
  containerAppBar: {
    flex: 1, // Adjust the flex value as needed
    backgroundColor: '#FFFFFF', // Change the background color of the container
   
    
  },
  wrapContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingHorizontal: 20,
    
  },
  cardContainer: {
    flex: 1,
    marginBottom: 10,
  },
  card: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#CCD8EE',
    borderRadius: 8,
    backgroundColor: '#fff',
    overflow: 'hidden',
    alignItems: 'flex-start',
    flexDirection: 'row',

  },
  details: {
    flex: 1,
    marginTop:20,
    padding: 10,
    justifyContent: 'center',

  },
  iconContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: 'transparent', // Adjust the background color if needed
    padding: 10,
  },
  loadingIndicator: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    zIndex:50,
  },
});

export default Home;