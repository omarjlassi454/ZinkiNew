import React, { useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, TextInput, ScrollView, Modal, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import NumericInput from 'react-native-numeric-input'
import { WebView } from 'react-native-webview';
import {  Button } from 'react-native-paper';

const ProfileComponent = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    var [textFromDB, settextFromDB] = useState("jlassi@jlassi.com");
    var [password, setPassword] = useState("password");

    const handleModalVisible = () => {
        setModalVisible(!modalVisible);
    };

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const handlePayment = () => {
        // Add your logic for handling the payment action
    };

    const handleLogout = () => {
        // Add your logic for handling the logout action
    };

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.card}>
                    <Text style={styles.cardTitle}>Email</Text>
                    <TextInput
                        editable={false}
                        style={[styles.input, styles.emailInput]} // Add emailInput style
                        placeholder="Enter your email"
                        value={textFromDB}
                    />

                    <Text style={styles.cardTitle}>Password</Text>
                    <View style={styles.passwordContainer}>
                        <TextInput
                            editable={false}
                            style={[styles.input, styles.passwordInput]} // Add passwordInput style
                            placeholder="Enter your password"
                            secureTextEntry={!showPassword}
                            value={password}
                        />
                        <TouchableOpacity onPress={togglePasswordVisibility} style={styles.eyeIconContainer}>
                            <Icon name={showPassword ? 'eye-slash' : 'eye'} size={20} color="#6CA4FC" />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.card}>
                    <Text style={styles.cardTitle}>Speech Quota : 250/4000 </Text>
                    {/* Add your logic and UI elements for speech quota */}
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 10,
                    }}>
                        <Button
                            mode="outlined"


                            style={{
                                borderRadius: 20,
                                marginVertical: 8,
                                borderColor: "#6CA4FC",
                            }}
                            contentStyle={{ height: 40 }}
                            labelStyle={{ fontSize: 16 }}
                            textColor="#6CA4FC"
                        >
                            Add quota
                        </Button>
                    </View>
                </View>


                <View style={styles.card}>
                    <Text style={styles.cardTitle}>Transcription Quota : 8/15</Text>
                   
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 10,
                    }}>
                        <Button
                            mode="outlined"

                            
                            style={{
                                borderRadius: 20,
                                marginVertical: 8,
                                borderColor: "#6CA4FC",
                            }}
                            contentStyle={{ height: 40 }}
                            labelStyle={{ fontSize: 16 }}
                            textColor="#6CA4FC"
                        >
                            Add quota
                        </Button>
                    </View>
                </View>



                <View style={styles.card}>
                    <TouchableOpacity onPress={handleLogout} style={styles.logoutButton}>
                        <Icon name="sign-out" size={20} color="#6CA4FC" />
                        <Text style={styles.logoutButtonText}>Logout</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>



            <Modal transparent={true} visible={modalVisible} animationType="slide" >
                <View style={styles.previewModalContainer}>
                    <TouchableOpacity onPress={() => {
                        setModalVisible(false);


                    }}>
                        <View style={styles.separatorLine}>
                            {/* Add content for the separator line here */}
                        </View>
                    </TouchableOpacity>
                    <ProgressSteps
                        activeStepIconBorderColor="#6CA4FC"
                        completedStepIconColor="#6CA4FC"
                        completedProgressBarColor="#6CA4FC"
                        activeLabelColor="#6CA4FC"
                        activeStepNumColor="#2B3270"
                        completedStepNumColor="#6CA4FC" >

                        <ProgressStep
                            label="Document Type"
                            //onNext={onNextStep}
                            //onPrevious={onPrevStep}
                            nextBtnText="Next"
                            previousBtnText="Previous"
                            nextBtnTextStyle={styles.stepText}
                            previousBtnTextStyle={styles.stepText}
                            nextBtnStyle={styles.nextButton}
                            previousBtnStyle={styles.previousButton}
                        >
                            <View style={styles.stepContainer}>
                                <Text style={styles.stepLabel}>Choose a document type</Text>

                                <View style={styles.documentTypeContainer}>
                                    <View style={styles.documentTypeItem}>
                                        <Text style={styles.documentTypeText}>Manuscript document</Text>
                                        <NumericInput
                                            minValue={0}
                                            maxValue={10}
                                            step={1}
                                            valueType="integer"
                                            //type='up-down'
                                            iconSize={20}
                                            upDownButtonsBackgroundColor="#E5E5E5"
                                            upDownButtonsContainerStyle={styles.numericInputButtons}
                                            upDownButtonsTextStyle={styles.numericInputButtonText}
                                            upDownButtonsUpIconComponent={() => (
                                                <MaterialIcons name="arrow-drop-up" size={15} color="#2B3270" />
                                            )}
                                            upDownButtonsDownIconComponent={() => (
                                                <MaterialIcons name="arrow-drop-down" size={15} color="#2B3270" />
                                            )}
                                        />
                                    </View>

                                    <View style={styles.documentTypeItem}>
                                        <Text style={styles.documentTypeText}>Modern document</Text>
                                        <NumericInput type="plus-minus" />
                                    </View>

                                    <View style={styles.documentTypeItem}>
                                        <Text style={styles.documentTypeText}>Handwritten document</Text>
                                        <NumericInput type="plus-minus" />
                                    </View>

                                    <View style={styles.documentTypeItem}>
                                        <Text style={styles.documentTypeText}>Old document</Text>
                                        <NumericInput type="plus-minus" />
                                    </View>
                                </View>
                            </View>
                        </ProgressStep>





                        <ProgressStep
                            label="Image Source"
                            //onNext={onNextStep}
                            //onPrevious={onPrevStep}
                            nextBtnText="Next"
                            previousBtnText="Previous"
                            nextBtnTextStyle={styles.stepText}
                            previousBtnTextStyle={styles.stepText}
                            nextBtnStyle={styles.nextButton}
                            previousBtnStyle={styles.previousButton}
                        >
                            <View style={styles.stepContainer}>
                                <Text style={styles.stepLabel}>Choose an image source</Text>

                                <View style={{ flex: 1 }}>
                                    <WebView
                                        //source={{ uri: `${process.env.REACT_APP_PAYMEE}${payToken}` }}
                                        style={{ flex: 1 }}
                                    />
                                </View>
                            </View>
                        </ProgressStep>

                        <ProgressStep
                            label="Transcribe Image"
                            // onNext={onNextStep}
                            // onPrevious={onPrevStep}
                            finishBtnText="Transcribe"
                            previousBtnText="Previous"
                            nextBtnTextStyle={styles.stepText}
                            previousBtnTextStyle={styles.stepText}
                            nextBtnStyle={styles.nextButton}
                            previousBtnStyle={styles.previousButton}
                        //onSubmit={() => handleTranscribe()}
                        >
                            <View>


                            </View>
                        </ProgressStep>
                    </ProgressSteps>
                </View>
                {isLoading && (
                    <View style={styles.loadingIndicator}>
                        <ActivityIndicator size="large" color="#6CA4FC" />
                    </View>
                )}
            </Modal>








        </View>
    );
};

const styles = StyleSheet.create({

    emailInput: {
        color: '#000', // Modify the color for the email input
    },

    passwordInput: {
        color: '#000', // Modify the color for the password input
    },

    container: {
        flex: 1,
        padding: 16,
    },
    card: {
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 16,
        marginBottom: 16,
    },
    cardTitle: {
        fontSize: 16,
        fontWeight: 'normal',
        marginBottom: 8,
    },
    input: {
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 4,
        padding: 8,
        marginBottom: 16,
    },
    passwordContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
  
    eyeIconContainer: {
        marginLeft: 8,
    },
    paymentButton: {
        backgroundColor: '#6CA4FC',
        borderRadius: 4,
        padding: 12,
        width: 140,
        alignItems: 'center',
        marginTop: 8,
        justifyContent: 'center',
    },
    paymentButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
    },
    logoutButton: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    logoutButtonText: {
        color: '#888',
        marginLeft: 8,
        fontSize: 16,
    },
    previewModalContainer: {
        position: 'absolute',
        bottom: 0,
        left: 5,
        right: 5,
        height: '85%',
        backgroundColor: '#FFFFFF',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingHorizontal: 20,
        paddingVertical: 30,
        zIndex: 30,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4,
        elevation: 30,
    },

    separatorLine: {
        height: 3,
        width: '30%',
        backgroundColor: '#CCD8EE',
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    nextButton: {
        backgroundColor: 'transparent',
        borderColor: '#6CA4FC',
        // Change next button background color
    },
    previousButton: {
        backgroundColor: 'transparent',
        borderColor: '#6CA4FC',
    },
    stepText: {
        fontSize: 15,
        fontWeight: 'normal',
        color: '#6CA4FC'
    },
    buttonTextStyle: {
        color: '#6CA4FC', // Change button text color
    },
    documentTypeContainer: {
        marginTop: 20,
        justifyContent: 'space-between',
    },

    documentTypeItem: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },

    documentTypeText: {
        fontSize: 16,
        marginRight: 10,
    },
    stepLabel: {
        fontSize: 14,
        fontWeight: 'normal',
        marginTop: 10,
    },
});

export default ProfileComponent;
