import React, { useEffect, useState } from 'react';
import { Image, View, Text, StyleSheet, ScrollView, Pressable, TouchableOpacity, Alert , Modal} from 'react-native';
import SearchBar from './SearchBar';
import SQLite from 'react-native-sqlite-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import NavBottom from './NavBottom';
import noDataImage2 from '../assets/images/noData.png';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { TextInput, Button, Menu, Divider, Provider, Card, RadioButton, } from 'react-native-paper';
import { useNavigation } from "@react-navigation/native";

const History = () => {
    const [docs, setDocs] = useState([]);
    const [selectedCardIndex, setSelectedCardIndex] = useState(null);
    const [selectedCardIndexes, setSelectedCardIndexes] = useState([]);
    const [modalAbbVisible, setModalAbbVisible] = useState(false);
    const [docIds, setDocIds] = useState([]); // State to store the document IDs
    const navigation = useNavigation();
    const [iduser, setiduser] = useState([]); // State to store the document IDs


    const [names, setNames] = useState('');
    const [dates, setDates] = useState('');
    const [docid, setdocid] = useState('');

    const [types, setTypes] = useState('');
    const [selectAll, setSelectAll] = useState(false);
    const [inputValue, setInputValue] = useState(selectedCardIndexes.length === 1 ? names[selectedCardIndexes[0]].split('.')[0] : '');



    useEffect(() => {
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

        db.transaction((tx) => {
            tx.executeSql(
              'SELECT * FROM TOKEN',
              [],
              (_, result) => {
                const re = result.rows.raw();
                console.log('1114: ',  re[0].UserId);
                const userid =(re[0].UserId);
                console.log('userid::: ',  userid);
                setiduser(userid)

                console.log('111 length : ', re.length);
                console.log('Retrieved rows:', result.rows);
          
                // Access the retrieved data
                const tokens = result.rows.raw();
               //console.log('Retrieved tokens:', tokens);
                
                // Process and log each token
                tokens.forEach((token, index) => {
                  //console.log(`Token ${index + 1}:`, token);
                });
              },
              (_, error) => {
                console.log('Error retrieving tokens:', error);
              }
            );
          });
          
        if (selectedCardIndexes.length === 1) {
            setInputValue(names[selectedCardIndexes[0]].split('.')[0]);
        } else {
            setInputValue('');
        }
        
    }, [selectedCardIndexes,iduser]);
    console.log('iduseriduser : ', iduser);




    const fetchDocIds = () => {
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

        db.transaction((tx) => {
            tx.executeSql(
                'SELECT id FROM DOCS WHERE UserId = ?',
                [iduser],
                (tx, result) => {
                    const ids = [];

                    for (let i = 0; i < result.rows.length; i++) {
                        ids.push(result.rows.item(i).id);
                    }

                    setDocIds(ids);
                    console.log('docIds ===>', docIds);
                },
                (error) => {
                    console.log('Error executing SQL statement:', error);
                }
            );
        });
    };

    useEffect(() => {
        //fetchDocIds();
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

        db.transaction((tx) => {
            tx.executeSql(
              'SELECT * FROM DOCS ',
              [],
              (tx, results) => {
                const DOCS = results.rows.raw();
                console.log('DOCS here --->:', DOCS);
              },
              (error) => {
                console.log('Error executing SQL statement:', error);
              }
            );
          });
    }, []);

    const onSubmit = () => {
        if (selectedCardIndexes.length === 1) {
            const newName = inputValue + '.' + names[selectedCardIndexes[0]].split('.')[1]; // Concatenate the new name with the existing extension
            const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

            db.transaction((tx) => {
                tx.executeSql(
                    'UPDATE DOCS SET name = ? WHERE UserId = ? AND id = ?',
                    [newName, iduser, docIds[selectedCardIndexes[0]]],
                    (tx, result) => {
                        // Update operation successful
                        const updatedNames = [...names];
                        updatedNames[selectedCardIndexes[0]] = newName;
                        setNames(updatedNames);
                        setModalAbbVisible(false);
                        setInputValue(selectedCardIndexes.length === 1 ? newName.split('.')[0] : '');
                        setSelectedCardIndexes([]); // Deselect the card
                        console.log('new name ==>', newName)
                        console.log('doc id ==>', docIds[selectedCardIndexes[0]])
                        console.log('updatedNames tab ==>', names)
                        console.log('selectedCardIndexes ==>', selectedCardIndexes)
                        console.log('doc id ===>', docIds)
                    },
                    (error) => {
                        console.log('Error executing SQL statement:', error);
                    }
                );
            });
        }
    };


   



    const cancel = () => {
        setModalAbbVisible(false);


    };

    const handleCardPress = (index) => {
        let newSelectedCardIndexes;
        if (selectedCardIndexes.includes(index)) {
            // Deselect the card
            newSelectedCardIndexes = selectedCardIndexes.filter((item) => item !== index);
        } else {
            // Select the card
            newSelectedCardIndexes = [...selectedCardIndexes, index];
        }
        setSelectedCardIndexes(newSelectedCardIndexes);
    };

    
    const handleSelectAll = () => {
        if (selectedCardIndexes.length === docs.length) {
            // Deselect all cards
            setSelectedCardIndexes([]);
        } else {
            // Select all cards
            setSelectedCardIndexes(docs.map((_, index) => index));
        }
    };
    console.log('indexex==>', selectedCardIndexes)

    const handleDelete = () => {
        if (selectedCardIndexes.length > 0) {
            Alert.alert(
                'Confirm Delete',
                'Are you sure you want to delete the selected images?',
                [
                    {
                        text: 'Cancel',
                        style: 'cancel',
                    },
                    {
                        text: 'Delete',
                        style: 'destructive',
                        onPress: () => {
                            const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
                            db.transaction((tx) => {
                                selectedCardIndexes.forEach((index) => {
                                    const uri = docs[index];
                                    tx.executeSql(
                                        'DELETE FROM DOCS WHERE UserId = ? AND URI = ?',
                                        [iduser, uri],
                                        (tx, result) => {
                                            console.log('succesfully deleting selected cards');
                                        },
                                        (error) => {
                                            console.log('Error executing SQL statement:', error);
                                        }
                                    );
                                });

                                // Refresh the docs array to reflect the changes
                                const updatedDocs = docs.filter((_, i) => !selectedCardIndexes.includes(i));
                                setDocs(updatedDocs);
                                setSelectedCardIndexes([]);
                            });
                        },
                    },
                ]
            );
        }
    };


    const handleSearch = (searchText) => {
        // Perform search logic using the searchText
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

        // Execute the query based on the searchText
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT * FROM DOCS WHERE name LIKE ?',
                [`%${searchText}%`],
                (tx, result) => {
                    // Process the search results
                    // Update your state or perform any necessary actions
                    // For example, update the list of documents based on the search results
                    const searchResults = result.rows.raw();
                    setDocs(searchResults);
                },
                (error) => {
                    console.log('Error executing SQL statement:', error);
                }
            );
        });
    };

    useEffect(() => {
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT Name FROM DOCS WHERE UserId = ?',
                [iduser],
                (tx, results) => {
                    const { rows } = results;
                    const imageNames = [];
                    for (let i = 0; i < rows.length; i++) {
                        const { Name } = rows.item(i);
                        imageNames.push(Name);
                    }
                    setNames(imageNames);
                },
                (error) => {
                    console.log('Error executing SQL statement:', error);
                }
            );
        });
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT Date FROM DOCS WHERE UserId = ?',
                [iduser],
                (tx, results) => {
                    const { rows } = results;
                    const Dates = [];
                    for (let i = 0; i < rows.length; i++) {
                        const { Date } = rows.item(i);
                        Dates.push(Date);
                    }
                    setDates(Dates);
                },
                (error) => {
                    console.log('Error executing SQL statement:', error);
                }
            );
        });
        

        
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT Type FROM DOCS WHERE UserId = ?',
                [iduser],
                (tx, results) => {
                    const { rows } = results;
                    const Types = [];
                    for (let i = 0; i < rows.length; i++) {
                        const { Type } = rows.item(i);
                        Types.push(Type);
                    }
                    setTypes(Types);
                },
                (error) => {
                    console.log('Error executing SQL statement:', error);
                }
            );
        });
    }, []);
    const handleCardLongPress = (index) => {
        setSelectedCardIndex(index);
        console.log('selected one index --->',selectedCardIndex)
    };


    useEffect(() => {
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

        db.transaction((tx) => {
            tx.executeSql(
                'SELECT URI FROM DOCS WHERE UserId = ? ORDER BY id DESC',
                [iduser],
                (tx, result) => {
                    const imageURIs = [];
                    for (let i = 0; i < result.rows.length; i++) {
                        const row = result.rows.item(i);
                        imageURIs.push(row.URI);
                    }
                    setDocs(imageURIs);
                },
                (error) => {
                    console.log('Error executing SQL statement:', error);
                }
            );
        });
    }, []);

    return (

        <View style={styles.container}>
            <SearchBar></SearchBar>
            {docs.length === 0 ? (
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 150, }}>
                    <Image source={noDataImage2} style={{ width: 160, height: 160 }} />
                    <Text style={{ color: '#BFBFBF', fontSize: 12, textAlign: 'center' }}>There is no recent documents to display yet</Text>
                </View>
            ) : (
                <ScrollView>
                    <View style={styles.containerImages}>
                        {docs.map((uri, index) => (
                            <Pressable
                                key={index}
                                style={[
                                    styles.container,
                                    selectedCardIndexes.includes(index) && styles.selectedContainer,
                                ]}
                                //onPress={() => handleCardPress(index)}
                                onLongPress={() => setSelectedCardIndexes([index])}
                            >
                                <View style={styles.card} key={index}>
                                    <TouchableOpacity
                                        onPress={() => handleCardPress(index)}
                                        style={styles.checkedCircle}
                                    >
                                        {selectedCardIndexes.includes(index) && (
                                            <Icon name="checkmark-circle-outline" size={24} color="#3E8DFB" />
                                        )}
                                    </TouchableOpacity>
                                    <Image source={{ uri }} style={styles.image} />
                                    <View style={styles.details}>
                                        <Text style={styles.date}>{names[index]}</Text>
                                        <Text style={styles.name}>{types[index]} document</Text>
                                        <Text style={styles.name}>{dates[index]}</Text>
                                        <Text style={styles.name}>{docid[index]}</Text>
            
                                    </View>
                                    {!selectedCardIndexes.includes(index) && (
                                    <View style={styles.iconContainer}>
                                        <TouchableOpacity onPress={() => navigation.navigate('TextToSpeech') } >
                                        <Icon name="ios-arrow-redo-outline" size={22} color="#2B3270" />
                                        </TouchableOpacity>
                                    </View>
                                    )}
                                </View>
                            </Pressable>
                        ))}
                    </View>
                </ScrollView>
            )}
            {selectedCardIndexes.length > 0 ? (
                <View style={styles.bottomMenu}>
                    <TouchableOpacity style={styles.menuItem} onPress={handleSelectAll}>
                        <Icon name="checkmark-circle-outline" size={24} color="#000" />
                        <Text style={styles.menuText}>
                            {selectedCardIndexes.length === docs.length ? 'Deselect All' : 'Select All'}
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.menuItem}
                        disabled={selectedCardIndexes.length > 1}
                        onPress={() => setModalAbbVisible(true)}
                    >
                        <MaterialCommunityIcons
                            name="rename-box"
                            size={24}
                            color={selectedCardIndexes.length > 1 ? "gray" : "#000"}
                        />
                        <Text style={styles.menuText}>
                            Rename
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={handleDelete} style={styles.menuItem}>
                        <Icon name="md-trash-outline" size={24} color="#000" />
                        <Text style={styles.menuText}>Delete</Text>
                    </TouchableOpacity>
                </View>
            ) : (
                <NavBottom />
            )}
            <Modal animationType='slide' transparent={true} visible={modalAbbVisible}>
                <View style={styles.modalContainer}>
                    <View style={styles.modalContent}>
                        <View style={styles.modalHeader}>
                            <TouchableOpacity onPress={() => setModalAbbVisible(false)}>
                                <Icon name="close-circle-outline" size={24} color="gray" style={styles.modalCloseButton} />
                            </TouchableOpacity>
                            <Text style={styles.modalTitle}>Set a new image name </Text>
                        </View>
                        <View style={styles.modalBreakInputContainer}>
                            <View style={styles.sliderContainer}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TextInput
                                        placeholderTextColor="#000"
                                        onChangeText={(text) => setInputValue(text)}
                                        value={inputValue}
                                        style={styles.Modalinput}
                                        underlineColor="#FFF"
                                        activeUnderlineColor='#6CA4FC'
                                        multiline={true}
                                        numberOfLines={2}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={styles.modalButtonContainer}>
                            <TouchableOpacity style={[styles.modalButton, { backgroundColor: '#6CA4FC' }]} onPress={() => onSubmit()}>
                                <Text style={{ color: '#FFFFFF' }}>Submit</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.modalButton, { backgroundColor: '#EFEFF4' }]} onPress={() =>  cancel() }>
                                <Text style={{ color: '#2B3270' }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>

    );
};

const styles = StyleSheet.create({
    containerImages: {
        flex: 1,
        padding: 10,
        marginBottom: 50,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    image: {
        width: 100,
        height: 100,
        marginRight: 10,
    },
    details: {
        flex: 1,
    },
    date: {
        fontSize: 14,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    name: {
        fontSize: 14,
    },
    selectionCircle: {
        position: 'absolute',
        top: 10,
        left: 10,
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: 'red',
        zIndex: 1,
    },
    card: {
        backgroundColor: 'rgba(188, 212, 243, 0.3)',
        padding: 10,
        width: '95%',
        alignSelf: 'center',
        height: 150,
        marginTop: 25,
        justifyContent: 'space-between',
        borderRadius: 20,
        flexDirection: 'row',
        marginBottom: 10,
        alignItems: 'center',

    },
    selectedContainer: {
        backgroundColor: '#CCD8EE',
        borderRadius: 30,
        marginBottom: 10,

    },
    checkedCircle: {
        position: 'absolute',
        right: 10,
        bottom: 10,
        color: '#3E8DFB'
    },
    bottomMenu: {
        flexDirection: 'row', // Display items side by side
        justifyContent: 'space-between', // Add space between items
        alignItems: 'center', // Align items vertically in the center
        paddingHorizontal: 16, // Add horizontal padding
        marginTop: 16,
        marginBottom: 8,
    },
    menuItem: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    menuText: {
        marginLeft: 8, // Add left margin between icon and text
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContent: {
        width: '80%',
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 20,
    },
    modalTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: -20,
        color: 'gray'

    },

    modalButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    modalButton: {
        backgroundColor: '#2196F3',
        borderRadius: 10,
        paddingVertical: 8,
        paddingHorizontal: 12,
        marginLeft: 10,
        justifyContent: 'center'
    },
    modalButtonText: {
        color: 'white',
        fontSize: 14,
    },
    modalCloseButton: {
        top: -10,
        left: 230,
    },
    Modalinput: {
        flex: 1,
        paddingHorizontal: 10,
        fontSize: 16,
        width: '80%', // set the width to 80% of the container
        backgroundColor: '#fff',
        color: '#fff',
        marginTop: 20,
        paddingVertical: 0,
        fontWeight: 'bold',
        justifyContent: 'space-between',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },
    iconContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        backgroundColor: 'transparent', // Adjust the background color if needed
        padding: 10,
    },
});

export default History;
