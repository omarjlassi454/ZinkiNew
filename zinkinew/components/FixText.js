import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { Button, ActivityIndicator, Text, TextInput, } from 'react-native-paper';
import SQLite from 'react-native-sqlite-storage';
import { useNavigation, useRoute } from '@react-navigation/native';
import NavBottom from './NavBottom'
const FixText = () => {
  const [loading, setLoading] = useState(false);
  const [defaultSSMLFromDB, setDefaultSSMLFromDB] = useState("");
  const [textFromDB, setTextFromDB] = useState("");
  const [urlFromDB, setUrlFromDB] = useState("");
  const [textError, setTextError] = useState(null);
  const navigation = useNavigation();
  const route = useRoute();
  const { documentId, userId } = route.params;
  const txt = 'وكذلك عبرة من قصة العائلة رايمر في ألا تغتر بالألقاب والشهادات وشدة الذكاء والشخصيات الكارزمية إذا كانت تخدم الباطل.'
  useEffect(() => {
    // Open the database
    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

    // Execute a SELECT query on Docs
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM DOCS WHERE id = ? AND UserId = ?',
        [documentId, userId],
        (tx, results) => {
          const users = results.rows.raw();
          const defaultSSML = results.rows.raw()[0]?.DefaultSSMl ?? "";
          const text = results.rows.raw()[0]?.Text ?? "";
          const url = results.rows.raw()[0]?.Url ?? "";
          // Set the retrieved values in the state
          setDefaultSSMLFromDB(defaultSSML);
          setTextFromDB(text);
          console.log("texttexttext", text)
          console.log("textFromDBtextFromDB", textFromDB)
          setUrlFromDB(url.replace(/\s+/g, ''));
        },
        (error) => {
          console.log('Error executing Docs SQL statement:', error);
        }
      );
    });
  }, []);
  console.log("textFromDBtextFromDB1 ", textFromDB)
  const isArabicText = (text) => {
    const arabicCharacters = text.match(/[\u0600-\u06FF]/g) || [];
    const arabicCharactersWithoutDiacritics = arabicCharacters.map((char) =>
      char.replace(/[\u064B-\u065F\u0670]/g, '')
    );

    const arabicPercentage = (arabicCharactersWithoutDiacritics.length / text.length) * 100;
    return arabicPercentage >= 20;
  };

  const handleUpdateText = () => {
    // Check if the text is empty
    if (textFromDB.trim() === '') {
      setTextError('Please enter some text');
      return;
    }

    // Open the database
    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

    // Update the text in the database
    db.transaction((tx) => {
      tx.executeSql(
        'UPDATE DOCS SET Text = ?, DefaultSSMl = ? WHERE id = ? AND UserId = ?',
        [textFromDB, `<speak>${textFromDB} </speak>`, documentId, userId], // Replace with the actual values of id and UserId
        (tx, result) => {
          console.log('Text updated successfully');
          Alert.alert(
            "Update Success!",
            "Let's go modify our voice",
            [
              {
                text: "Go",
                onPress: () => navigation.navigate('TextToSpeech', {
                  documentId: documentId,
                  previousRoute: 'FixText'
                })
              },
              {
                text: "Cancel",
                style: "cancel"
              }
            ]
          );
        },
        (error) => {
          console.log('Error updating text:', error);
        }
      );
    });
  };

  const handleIchkilText = () => {
    // Check if the input text is primarily Arabic
    const isArabic = isArabicText(textFromDB);

    if (isArabic) {
      setLoading(true); // Set loading state to true

      // Send a POST request to the API endpoint
      fetch('https://vby7mr2rvr7tazurlutd6b55i40dlsgb.lambda-url.us-east-1.on.aws', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          text: textFromDB,
        }),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Something went wrong');
          }
        })
        .then((data) => {
          // Update the input text with the new value
          setTextFromDB(data.result);
          console.log('Text updated successfully');
        })
        .catch((error) => {
          console.error('Errorupdating text:', error);
        })
        .finally(() => {
          setLoading(false); // Set loading state back to false
        });
    } else {
      console.log('The input text is not primarily Arabic.');
    }
  };

  return (
    <View style={styles.container}>
      <TextInput

        value={textFromDB}
        onChangeText={(newText) => {
          setTextFromDB(newText);
          setTextError(null);
        }}
        textAlignVertical="top"
        placeholder="Enter text here..."
        placeholderTextColor="#ccc"
        selectionColor='#89CFF0'
        activeUnderlineColor='transparent'
        mode="flat"
        dense
        style={styles.input}
        underlineColor="#FFF"
        numberOfLines={20}
        multiline={true}
      />
      {textError && <Text style={styles.error}>{textError}</Text>}
      <View style={styles.buttonContainer}>
  {isArabicText(textFromDB) ? (
    <Button
      mode="outlined"
      textColor="#6CA4FC"
      style={[styles.button, {
        borderRadius: 20,
        marginVertical: 8,
        borderColor: "#6CA4FC",
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2
      }]}
      onPress={handleIchkilText}
      loading={loading}
      disabled={loading}
    >
      Add diacritics
    </Button>
  ) : (
    <Button
      mode="outlined"
      textColor="#6CA4FC"
      style={[styles.button, {
        borderRadius: 20,
        marginVertical: 8,
        borderColor: "#6CA4FC",
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2
      }]}
      disabled={true} // Disable the button
    >
      Add diacritics
    </Button>
  )}

  <Button
    mode="outlined"
    textColor="#6CA4FC"
    style={[styles.button, {
      borderRadius: 20,
      marginVertical: 8,
      borderColor: "#6CA4FC",
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2
    }]}
    disabled={loading}
    onPress={handleUpdateText}
  >
    Confirm
  </Button>
</View>
<NavBottom></NavBottom>
    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },

  buttonContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  button: {
    width: '48%',
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
  input: {
    backgroundColor: '#fff',
    color: '#fff',
    marginTop: 20,
    paddingHorizontal: 10,
    paddingVertical: 10,
    fontSize: 16,
    fontWeight: 'bold',
    justifyContent: 'space-between',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
});

export default FixText;