import IpAdress from './IpAdress';
import React, { useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image, ScrollView, ToastAndroid  } from 'react-native';
import { TextInput, Button, Headline, Icon } from 'react-native-paper';
import { Colors } from '../canstant/index'
import { useNavigation } from "@react-navigation/native";

const SignUp = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [username, setusername] = useState('');
  const navigation = useNavigation();

  const handleSignUp = () => {
    const userData = {
      username,
      email,
      password,
      firstName,
      lastName,
      mobileNumber,
      birthDate,
    };

    fetch(`${IpAdress.IP}/auth/signup`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userData),
    })
      .then((response) => {
        if (response.ok) {
          ToastAndroid.show('User successfully registered!', ToastAndroid.SHORT);
          navigation.navigate('Login')
          console.log('User successfully registered on the backend');

          // Handle successful registration
        } else {
          console.error('Error registering user on the backend');
          // Handle registration error
        }
      })
      .catch((error) => {
        console.error('Error registering user on the backend:', error);
        // Handle registration error
      });
  };

  return (

    <View style={styles.container}>
      <ScrollView showsHorizontalScrollIndicator={true}>
      <Image
            source={require('../assets/logo1.png')}
            style={{ alignSelf: 'center', marginTop: 250 , width: 170 ,height: 200 }}

          />
          <Headline style={styles.title}>Create New Account</Headline>
      <View style={styles.imageContainer}>
      </View>
        <View style={styles.inputContainer}>


          <TextInput
            style={styles.input}
            placeholder="username"
            value={username}
            onChangeText={setusername}
            right={<TextInput.Icon
              icon={'account-outline'}
              iconColor="#6CA4FC"
            />}

            mode='outlined'
            //keyboardType='Name'
            autoCapitalize='none'
            autoCorrect={false}
            theme={{
              colors: {
                primary: Colors.primary,
                background: 'white',
                text: 'black',

              }
            }}
          />
        </View>
        <View style={styles.inputContainer}>

          <TextInput
            style={styles.input}
            placeholder="Email"
            value={email}
            onChangeText={setEmail}

            right={<TextInput.Icon
              icon={'email-outline'}
              iconColor="#6CA4FC"
            />}

            mode='outlined'
            //keyboardType='email-address'
            autoCapitalize='none'
            autoCorrect={false}
            theme={{
              colors: {
                primary: Colors.primary,
                background: 'white',
                text: 'black',

              }
            }}
          />
        </View>
        <View style={styles.inputContainer}><TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry
          value={password}
          onChangeText={setPassword}
          mode="outlined"
          right={<TextInput.Icon icon="lock" iconColor="#6CA4FC" />}
          autoCapitalize='none'
          autoCorrect={false}
          theme={{
            colors: {
              primary: Colors.primary,
              background: 'white',
              text: 'black'
            }
          }}
        /></View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="First Name"
            value={firstName}
            onChangeText={setFirstName}
            right={<TextInput.Icon
              icon={'account-outline'}
              iconColor="#6CA4FC"
              //keyboardType='Name'
              autoCapitalize='none'
              autoCorrect={false}
              theme={{
                colors: {
                  primary: Colors.primary,
                  background: 'white',
                  text: 'black',

                }
              }}
            />}

            mode='outlined'
          />

        </View>

        <View style={styles.inputContainer}>

          <TextInput
            style={styles.input}
            placeholder="Last Name"
            value={lastName}
            onChangeText={setLastName}
            right={<TextInput.Icon
              icon={'account-outline'}
              iconColor="#6CA4FC"
              //keyboardType='Name'
              autoCapitalize='none'
              autoCorrect={false}
              theme={{
                colors: {
                  primary: Colors.primary,
                  background: 'white',
                  text: 'black',

                }
              }}
            />}

            mode='outlined'

          />

        </View>
        <View style={styles.inputContainer}><TextInput
          style={styles.input}
          placeholder="Mobile Number"
          value={mobileNumber}
          onChangeText={setMobileNumber}
          right={<TextInput.Icon
            icon={'phone-outline'}
            iconColor="#6CA4FC"
           // keyboardType='phone-Number'
            autoCapitalize='none'
            autoCorrect={false}
            theme={{
              colors: {
                primary: Colors.primary,
                background: 'white',
                text: 'black',

              }
            }}
          />}

          mode='outlined'
        />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Birth Date (yyyy-mm-dd)"
            value={birthDate}
            onChangeText={setBirthDate}
            right={<TextInput.Icon
            icon={'calendar-outline'}
            iconColor="#6CA4FC"
            //keyboardType='calendar-Number'
            autoCapitalize='none'
            autoCorrect={false}
            theme={{
              colors: {
                primary: Colors.primary,
                background: 'white',
                text: 'black',

              }
            }}
          />}

          mode='outlined'
          />
        </View>




        <View style={styles.registerButtonSection}>

        <Button  mode="outlined"


style={{
    borderRadius: 20,
    marginVertical: 8,
    borderColor: "#6CA4FC",
}}
contentStyle={{ height: 40 }}
labelStyle={{ fontSize: 16 }}
textColor="#6CA4FC"   onPress={handleSignUp}>
            <Text style={styles.buttonText}>Sign Up</Text>

          </Button>

          <Button
            mode='text'
            onPress={() => navigation.navigate('Login')}
            labelStyle={{ color: 'blue', fontSize: 14 }}>
            already have an account ? Sign In Here
          </Button>
        </View>
      </ScrollView>

    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
    marginTop: -200
  },
  title: {
    textAlign: 'center',
    marginTop: 29,
    marginBottom: 29,
    color: Colors.primary,
    fontSize: 30,


  },
  inputContainer: {
    marginVertical: 8,
  },
  registerButtonSection: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50
  },
  textButtonSection: {

  },
  imageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,


  },
  input: {
    width: '100%',
    backgroundColor: 'white',

  },
  button: {
    marginVertical: 8,
    width: '75%',
    backgroundColor: Colors.primary,
    borderRadius: 15,




  },

});


export default SignUp;
