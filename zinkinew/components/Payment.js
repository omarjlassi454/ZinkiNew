import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { WebView } from 'react-native-webview';
import PropTypes from 'prop-types';
import { useNavigation, useRoute } from '@react-navigation/native';


// ...
const Payment = () => {
    const route = useRoute();
    const navigation = useNavigation();
    const { token } = route.params;
    console.log('paymee screen shows up from paymee');
    const handleGoBackButton = () => {
        navigation.goBack();
    };
    return <><Button title='go back' onPress={handleGoBackButton} /><WebView source={{ uri: `https://sandbox.paymee.tn/gateway/${token}` }} style={{ flex: 1 }} /></>

    
};


export default Payment;