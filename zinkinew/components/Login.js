import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet, Image, Text } from 'react-native';
import IpAdress from './IpAdress';
import { useNavigation } from "@react-navigation/native";
import { RNKeycloak } from '@react-keycloak/native';
import { ReactNativeKeycloakProvider } from '@react-keycloak/native';
import SQLite from 'react-native-sqlite-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import log from '../assets/images/log.png';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Login = () => {
  const [username, setusername] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();
  const [activeInput, setActiveInput] = useState('');

  const handleLogin = async () => {
    // Perform login logic using Keycloak or any authentication mechanism
    try {
      // Make a request to your backend API to authenticate the user
      const response = await fetch(`${IpAdress.IP}/auth/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password }),
      });
      const responseData = await response.json();
      console.log('responseData :::::', responseData);

      if (response.ok) {
        console.log('TOKEN:', responseData.accessToken);
      
        const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
      
        // Retrieve user details from MongoDB and insert/update the single row in the TOKEN table
        db.transaction((tx) => {
          tx.executeSql(
            'CREATE TABLE IF NOT EXISTS TOKEN (token TEXT, UserId TEXT)',
            [],
            () => {
              // Table created or already exists
              tx.executeSql(
                'SELECT COUNT(*) FROM TOKEN',
                [],
                (_, resultSet) => {
                  const { rows } = resultSet;
                  const rowCount = rows.item(0)['COUNT(*)'];
                  if (rowCount === 0) {
                    // Insert a new row since there are no existing rows
                    tx.executeSql(
                      'INSERT INTO TOKEN (token, UserId) VALUES (?, ?)',
                      [responseData.accessToken, responseData.UserId],
                      (_, result) => {
                        if (result.rowsAffected > 0) {
                          console.log('Token and UserId inserted successfully');
                          // Additional code here if needed
                        } else {
                          console.log('No rows were affected');
                        }
                      },
                      (error) => {
                        console.log('Error inserting token:', error);
                      }
                    );
                  } else {
                    // Update the existing row
                    tx.executeSql(
                      'UPDATE TOKEN SET token = ?, UserId = ?',
                      [responseData.accessToken, responseData.UserId],
                      (_, result) => {
                        if (result.rowsAffected > 0) {
                          console.log('Token and UserId updated successfully');
                          // Additional code here if needed
                        } else {
                          console.log('No rows were affected');
                        }
                      },
                      (error) => {
                        console.log('Error updating token:', error);
                      }
                    );
                  }
                },
                (error) => {
                  console.log('Error counting rows in TOKEN:', error);
                }
              );
            },
            (error) => {
              console.log('Error creating table:', error);
            }
          );
        });
      
        // Login successful, retrieve user data from response if needed
        // Redirect to the home page or any other protected route
        navigation.navigate('Home');
      }
      
       else {
        // Login failed, handle error (e.g., display an error message)
        console.error('Login failed');
      }
    } catch (error) {
      console.error('Error during login:', error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={log} style={styles.logo} />
        
      </View>
      <View style={styles.card}>
        
        <View style={[styles.inputContainer, activeInput === 'username' && styles.activeInput]}>
          <Icon name="person" size={24} color="#3E8DFB" style={styles.icon} />
          <TextInput
            style={styles.input}
            placeholder="Username"
            placeholderTextColor="#999"
            onChangeText={(text) => setusername(text)}
            onFocus={() => setActiveInput('username')}
            onBlur={() => setActiveInput('')}
          />
        </View>
        <View style={[styles.inputContainer, activeInput === 'password' && styles.activeInput]}>
          <Icon name="lock-closed" size={24} color="#3E8DFB" style={styles.icon} />
          <TextInput
            style={styles.input}
            placeholder="Password"
            placeholderTextColor="#999"
            secureTextEntry
            onChangeText={(text) => setPassword(text)}
            onFocus={() => setActiveInput('password')}
            onBlur={() => setActiveInput('')}
          />
        </View>
        <Button title="Login" onPress={handleLogin} color="#3E8DFB" style={styles.button} />
        <TouchableOpacity  onPress={() => navigation.navigate('SignUp')}>
        <Text style={styles.signupText} >Don{"'"}t have an account?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
    backgroundColor: '#CCD8EE',
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -60,
    marginBottom:80,
  },
  logo: {
    width: 100,
    height: 120,
  },
  welcomeText: {
    fontSize: 13,
    color: '#555',
    marginTop: 16,
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 3,
    padding: 16,
    height: 300,
    justifyContent: 'center',
    marginTop: -40,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    paddingBottom: 4,
    height: 50,
  },
  activeInput: {
    borderBottomColor: '#3E8DFB',
  },
  icon: {
    marginRight: 8,
  },
  input: {
    flex: 1,
    height: 40,
    fontSize: 16,
    color: '#333',
  },
  button: {
    borderRadius: 20,
  },
  signupText: {
    fontSize: 14,
    color: '#999',
    marginTop: 16,
    textAlign: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
   
    textAlign: 'center',
    color: '#3E8DFB',
  },
});
export default Login;
