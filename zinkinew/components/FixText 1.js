import React, { useState , useEffect} from 'react';
import { View, TextInput, StyleSheet , Alert} from 'react-native';
import { Button, ActivityIndicator, Text } from 'react-native-paper';
import SQLite from 'react-native-sqlite-storage';
import { useNavigation } from "@react-navigation/native";

const FixText = () => {
  const [loading, setLoading] = useState(false);
  const [defaultSSMLFromDB, setDefaultSSMLFromDB] = useState("");
  const [textFromDB, setTextFromDB] = useState("");
  const [urlFromDB, setUrlFromDB] = useState("");
  const [textError, setTextError] = useState(null);
  const navigation = useNavigation();
  const txt='وكذلك عبرة من قصة العائلة رايمر في ألا تغتر بالألقاب والشهادات وشدة الذكاء والشخصيات الكارزمية إذا كانت تخدم الباطل.'
  useEffect(() => {
    // Open the database
    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });
  
    // Execute a SELECT query on Docs
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM DOCS WHERE id = ? AND UserId = ?',
        [16, 6],
        (tx, results) => {
          const users = results.rows.raw();
          const defaultSSML = results.rows.raw()[0]?.DefaultSSMl ?? "";
          const text = results.rows.raw()[0]?.Text ?? "";
          const url = results.rows.raw()[0]?.Url ?? "";
// Set the retrieved values in the state
          setDefaultSSMLFromDB(defaultSSML);
          setTextFromDB(text);
          setUrlFromDB(url.replace(/\s+/g, ''));
        },
        (error) => {
          console.log('Error executing Docs SQL statement:', error);
        }
      );
    });
  }, []);
  
  const isArabicText = (text) => {
    const arabicCharacters = text.match(/[\u0600-\u06FF]/g) || [];
    const arabicCharactersWithoutDiacritics = arabicCharacters.map((char) =>
      char.replace(/[\u064B-\u065F\u0670]/g, '')
    );
    
    const arabicPercentage = (arabicCharactersWithoutDiacritics.length / text.length) * 100;
    return arabicPercentage >= 20;
  };

  const handleUpdateText = () => {
    // Check if the text is empty
    if (textFromDB.trim() === '') {
      setTextError('Please enter some text');
      return;
    }

    // Open the database
    const db = SQLite.openDatabase({ name: 'ZinkiDB', location: 'default' });

    // Update the text in the database
    db.transaction((tx) => {
      tx.executeSql(
        'UPDATE DOCS SET Text = ?, DefaultSSMl = ? WHERE id = ? AND UserId = ?',
        [textFromDB, `<speak>${textFromDB} <speak>`, 16, 6], // Replace with the actual values of id and UserId
        (tx, result) => {
          console.log('Text updated successfully');
          Alert.alert(
            "Update Success!",  
            "Let's go modify our voice",   
            [
              {
                text: "Go",  
                onPress: () => navigation.navigate('TextToSpeech', {
                  backButtonVisible: false
                }) 
              },  
              {
                text: "Cancel",
                style: "cancel" 
              }
            ]  
          );
        },
        (error) => {
          console.log('Error updating text:', error);
        }
      );
    });
  };

  const handleIchkilText = () => {
    // Check if the input text is primarily Arabic
    const isArabic = isArabicText(textFromDB);

    if (isArabic) {
      setLoading(true); // Set loading state to true

      // Send a POST request to the API endpoint
      fetch('https://vby7mr2rvr7tazurlutd6b55i40dlsgb.lambda-url.us-east-1.on.aws', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          text: textFromDB,
        }),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Something went wrong');
          }
        })
        .then((data) => {
          // Update the input text with the new value
          setTextFromDB(data.result);
          console.log('Text updated successfully');
        })
        .catch((error) => {
          console.error('Errorupdating text:', error);
        })
        .finally(() => {
          setLoading(false); // Set loading state back to false
        });
    } else {
      console.log('The input text is not primarily Arabic.');
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={[styles.input, { 
          fontFamily: 'Helvetica', fontSize: 20,
          borderColor: 'blue', borderWidth: 2,  
          borderRadius: 10,  
          shadowColor: '#000',  
          shadowOffset: { width: 0, height: 5},   
          shadowOpacity: 0.8, 
          shadowRadius: 2 
        }]}
        value={textFromDB}
        onChangeText={(newText) => {
          setTextFromDB(newText);
          setTextError(null); 
        }}
        placeholder="Enter text here"
        multiline
        textAlignVertical="top"
      />
      {textError && <Text style={styles.error}>{textError}</Text>}
      <View style={styles.buttonContainer}>
        {isArabicText(textFromDB) && (
          <Button
            mode="contained" 
            buttonColor ="blue"
            style={[styles.button, {  
              borderColor: 'blue', borderWidth: 2,  
              borderRadius: 10,  
              shadowColor: '#000',  
              shadowOffset: { width: 0, height: 2 },   
              shadowOpacity: 0.8,  
              shadowRadius: 2 
            }]}
            onPress={handleIchkilText}
            loading={loading}
            disabled={loading}  
          >
            Ichkil Text
          </Button>
        )}
        <Button
          mode="contained"
          buttonColor ="blue"
          style={[styles.button, {   
            borderColor: 'blue', borderWidth: 2,  
            borderRadius: 10,  
            shadowColor: '#000',  
            shadowOffset: { width: 0, height: 2 },    
            shadowOpacity: 0.8,  
            shadowRadius: 2  
          }]}
                disabled={loading}  

          onPress={handleUpdateText} 
        >
          Confirm
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  input: {
    flex: 1,
    borderWidth: 1, 
    borderColor: '#ccc',
    borderRadius: 4,
    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 20,
    lineHeight: 30, 
    textAlignVertical: 'top',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  button: {
    width: '40%',
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
});

export default FixText;