import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet ,ToastAndroid  } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import IpAdress from './IpAdress';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from "@react-navigation/native";

const SignUp = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [username, setusername] = useState('');
  const [activeInput, setActiveInput] = useState('');
  const navigation = useNavigation();

  const handleSignUp = () => {
    const userData = {
      username,
      email,
      password,
      firstName,
      lastName,
      mobileNumber,
      birthDate,
    };

    fetch(`${IpAdress.IP}/auth/signup`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userData),
    })
      .then((response) => {
        if (response.ok) {
          ToastAndroid.show('User successfully registered!', ToastAndroid.SHORT);
          navigation.navigate('Login')
          console.log('User successfully registered on the backend');
          // Handle successful registration
        } else {
          console.error('Error registering user on the backend');
          // Handle registration error
        }
      })
      .catch((error) => {
        console.error('Error registering user on the backend:', error);
        // Handle registration error
      });
  };

  const handleInputFocus = (inputName) => {
    setActiveInput(inputName);
  };

  const handleInputBlur = () => {
    setActiveInput('');
  };

  return (
    <View style={styles.container}>
    <ScrollView>
      <View style={styles.card}>
        <Text style={styles.title}>Sign Up</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.input,
              activeInput === 'username' && styles.activeInput,
            ]}
            placeholder="Username"
            value={username}
            onChangeText={setusername}
            onFocus={() => handleInputFocus('username')}
            onBlur={handleInputBlur}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.input,
              activeInput === 'email' && styles.activeInput,
            ]}
            placeholder="Email"
            value={email}
            onChangeText={setEmail}
            onFocus={() => handleInputFocus('email')}
            onBlur={handleInputBlur}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.input,
              activeInput === 'password' && styles.activeInput,
            ]}
            placeholder="Password"
            secureTextEntry
            value={password}
            onChangeText={setPassword}
            onFocus={() => handleInputFocus('password')}
            onBlur={handleInputBlur}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.input,
              activeInput === 'firstName' && styles.activeInput,
            ]}
            placeholder="First Name"
            value={firstName}
            onChangeText={setFirstName}
            onFocus={() => handleInputFocus('firstName')}
            onBlur={handleInputBlur}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.input,
              activeInput === 'lastName' && styles.activeInput,
            ]}
            placeholder="Last Name"
            value={lastName}
            onChangeText={setLastName}
            onFocus={() => handleInputFocus('lastName')}
            onBlur={handleInputBlur}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.input,
              activeInput === 'mobileNumber' && styles.activeInput,
            ]}
            placeholder="Mobile Number"
            value={mobileNumber}
            onChangeText={setMobileNumber}
            onFocus={() => handleInputFocus('mobileNumber')}
            onBlur={handleInputBlur}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.input,
              activeInput === 'birthDate' && styles.activeInput,
            ]}
            placeholder="Birth Date (yyyy-mm-dd)"
            value={birthDate}
            onChangeText={setBirthDate}
            onFocus={() => handleInputFocus('birthDate')}
            onBlur={handleInputBlur}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={handleSignUp}>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => navigation.navigate('Login')}>
          <Text style={styles.signupText}>Already have an account</Text></TouchableOpacity>
      </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
    backgroundColor: '#CCD8EE',
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 3,
    padding: 16,
    height: 580,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
    color: '#3E8DFB',
  },
  inputContainer: {
    marginBottom: 20,
  },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 12,
    backgroundColor: '#fff',
  },
  activeInput: {
    borderColor: '#3E8DFB', // Change color for active input
  },
  button: {
    backgroundColor: '#3E8DFB',
    borderRadius: 8,
    paddingVertical: 12,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
  },
  signupText: {
    fontSize: 14,
    color: '#999',
    marginTop: 5,
    textAlign: 'center',
  },
});


export default SignUp;
